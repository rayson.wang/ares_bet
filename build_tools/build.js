/* eslint-disable @typescript-eslint/no-var-requires */
// add Cocos Creator engine exe in System environment [Path] variables .
// ex: C:\CocosDashboard_1.0.8\resources\.editors\Creator\2.3.3
const shell = require('shelljs');
const { program } = require('commander');
const fs = require('fs');

// console.log('process.argv', process.argv);
program
    .option('-d, --debug [type]', 'Is enable debug mode, preset is disable', 'false')
    .option('-p, --platform [type]', 'Is build platform target, preset is web-mobile', 'web-mobile')
    .option('-t, --deploy [type]', 'Is deploy target, preset is null', '')
    .option('-t, --target [type] ', 'Is deploy target, preset is null', 'games/game_common')
    .option('-c, --custom [type]', 'Is custom build params of creator, preset is empty obj', '{}');
program.parse();

const options = program.opts();
console.log('options', options);

// set build settings
if (options.deploy) {
    const target = `${__dirname}/../assets/${options.target}/Setting.json`;
    let settingJson = require(`${__dirname}/env_settings/${options.deploy}/Setting.json`);
    settingJson.env = options.deploy;
    const settingData = JSON.stringify(settingJson);
    fs.writeFileSync(target, settingData);
}

// buildCMD
let buildCMD = `platform=${options.platform};debug=${options.debug};`;

if (options.platform === 'web-desktop') {
    buildCMD += 'previewWidth=460;previewHeight=820;';
}

// custom
const custom = JSON.parse(program.custom);
for (const [key, value] of Object.entries(custom)) {
    buildCMD += `${key}=${value};`;
}

console.log('buildCMD', buildCMD);

let cmds = [];
cmds.push(`CocosCreator --nologin --path .. --build ${buildCMD}`);
cmds.forEach((cmd) => {
    shell.exec(cmd);
});
