// jest.config.js
// Sync object

// require('./creator.d.ts');

module.exports = {
    preset: 'ts-jest',
    transform: {
        '\\.ts$': ['ts-jest'],
        '^.+\\.[j]sx?$': 'babel-jest',
    },
    testEnvironment: 'node',
    collectCoverageFrom: ['<rootDir>/assets/**/*.ts'],
    testPathIgnorePatterns: ['temp/vscode-dist'],
    setupFiles: [
        'jest-canvas-mock', // npm 套件只需要名稱
        '<rootDir>/tests/engine/cocos2d-js-for-preview.js',
    ],
    testEnvironment: 'jsdom',
    transformIgnorePatterns: ['cocos2d-js-for-preview.js'],
};
