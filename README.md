# ares-bet

## setup

```shell
npm i -D
```

## Update submodule

```bash
git submodule update --init --recursive # --remote (if you want to update to latest)
git submodule sync
```

## Switch config

```bash
cd build_tools
node build.js --deploy "SIT" --target "pandora"
```
