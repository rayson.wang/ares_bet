import { AresLobbyStoreBase } from '../../ares_base/AresBaseIndex';
import { observable, render, runInAction } from '../../shared_frameworks/SharedFrameworksIndex';
import { AresLobbyControllerBase, rootStore } from '../LobbyIndex';

const { ccclass, property } = cc._decorator;

@ccclass
export class TableListController extends cc.Component {
    store: AresLobbyStoreBase;

    // LIFE-CYCLE CALLBACKS:

    onLoad(): void {
        super.onLoad && super.onLoad();
    }

    // update (dt) {}
}
