import { TableStore } from '../../ares_base/store/TableStore';
import BaccaratRoadMapController from '../../components/BaccaratRoadMap/BaccaratRoadMapController';
import { observer, render, runInAction } from '../../shared_frameworks/SharedFrameworksIndex';

const { ccclass, property } = cc._decorator;

@ccclass
@observer
export class TableSelect extends cc.Component {
    @property(cc.Label) tableName: cc.Label = null;
    @property(cc.Label) tableId: cc.Label = null;

    @property(cc.Label) onlinePlayer: cc.Label = null;

    @property(cc.Node) countDownIcon: cc.Node = null;
    @property(cc.Node) speed: cc.Node = null;
    @property(cc.Label) countDown: cc.Label = null;
    @property(cc.Label) bRound: cc.Label = null;
    @property(cc.Label) rRound: cc.Label = null;
    @property(cc.Label) gRound: cc.Label = null;
    @property(cc.Node) roadMapRoot: cc.Node = null;
    @property(cc.Prefab) roadMapTemplate: cc.Prefab = null;

    roadMapCtrl: BaccaratRoadMapController = null;
    store: TableStore = null;
    gid: number = null;

    init(tableStore: TableStore): void {
        this.store = tableStore;
    }

    @render renderTableInfo(): void {
        const { tableName, isSpeed, state, tid, onlinePlayer, betCountdownSecInt } = this.store;

        this.tableName.string = tableName + ' (' + state + ')';
        this.tableId.string = '#' + tid?.toString();
        this.speed.active = isSpeed;
        this.onlinePlayer.string = onlinePlayer?.toString();
        this.countDown.string = !betCountdownSecInt ? '--' : betCountdownSecInt.toString();
        this.countDown.node.color = !betCountdownSecInt ? cc.color(255, 0, 0) : cc.color(255, 255, 255);
        this.countDownIcon.color = !betCountdownSecInt ? cc.color(255, 0, 0) : cc.color(8, 201, 187);
    }
    @render renderTotal(): void {
        const { total } = this.roadMapCtrl;
        if (total) {
            this.bRound.string = total[0].toString();
            this.rRound.string = total[1].toString();
            this.gRound.string = total[2].toString();
        }
    }
    @render renderRoadMap(): void {
        cc.log('renderRoadMap:', this.store.history.length);
        runInAction(() => {
            if (this.store.init) {
                if (!this.store.history.length) {
                    this.roadMapCtrl.history = [];
                } else {
                    this.roadMapCtrl?.history.push(this.store.history[this.store.history.length - 1].dieNumber);
                }
            } else {
                this.roadMapCtrl.history = this.store.history?.map(({ dieNumber }) => dieNumber);
            }
        });
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad(): void {
        const roadMapT = cc.instantiate(this.roadMapTemplate);
        this.roadMapCtrl = roadMapT.getComponent(BaccaratRoadMapController);

        this.roadMapRoot.addChild(roadMapT);

        // unregister default Events for scrollView
        this.roadMapCtrl.scrollViewBead._unregisterEvent();
        this.roadMapCtrl.scrollViewBig._unregisterEvent();
        this.roadMapCtrl.scrollViewBigEye._unregisterEvent();
        this.roadMapCtrl.scrollViewSmall._unregisterEvent();
        this.roadMapCtrl.scrollViewCockroach._unregisterEvent();
        this.roadMapCtrl.scrollViewBead._registerEvent = () => null;
        this.roadMapCtrl.scrollViewBig._registerEvent = () => null;
        this.roadMapCtrl.scrollViewBigEye._registerEvent = () => null;
        this.roadMapCtrl.scrollViewSmall._registerEvent = () => null;
        this.roadMapCtrl.scrollViewCockroach._registerEvent = () => null;

        this.store.history.map(({ dieNumber }) => {
            runInAction(() => {
                this.roadMapCtrl?.history.push(dieNumber);
            });
        });
    }
}
