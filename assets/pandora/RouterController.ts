import { observer } from '../shared_frameworks/SharedFrameworksIndex';
const { ccclass, property } = cc._decorator;

@ccclass
@observer
export default class RouterController extends cc.Component {
    @property(cc.Label)
    label: cc.Label = null;

    @property
    text = 'hello';

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    // update (dt) {}
}
