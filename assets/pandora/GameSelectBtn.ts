import LobbyStore from './LobbyStore';

const { ccclass } = cc._decorator;

@ccclass
export default class GameSelectBtn extends cc.Component {
    store: LobbyStore = null;
    gid: number = null;

    async init(gid: number, lobbyStore: LobbyStore): Promise<void> {
        this.gid = gid;
        this.store = lobbyStore;
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad(): void {
        this.node.on('click', () => {});
    }
}
