import { runInAction, observable, observer, render, rootStore, Util, lib } from '../LobbyIndex';

type Breadcrumb = {
    dateTime: string;
    gid?: number | undefined;
    tid?: number | undefined;
    rid?: number | undefined;
};
const { ccclass, property } = cc._decorator;
@ccclass
@observer
export default class PlayerHistoryController extends cc.Component {
    @property(cc.Node) dateData: cc.Node = null;
    @property(cc.Node) winLossByGame: cc.Node = null;
    @property(cc.Node) tableData: cc.Node = null;
    @property(cc.Node) roundsData: cc.Node = null;
    @property(cc.Node) oneRound: cc.Node = null;
    @property(cc.Node) headRoot: cc.Node = null;
    @property(cc.Node) bodyRoot: cc.Node = null;
    @property(cc.Node) roundRoot: cc.Node = null;
    @property(cc.Node) totalRoot: cc.Node = null;
    @property(cc.Node) resultRoot: cc.Node = null;
    @property(cc.Node) tableView: cc.Node = null;

    @property(cc.ScrollView) resultScrollView: cc.ScrollView = null;
    @property(cc.ScrollView) tableScrollView: cc.ScrollView = null;
    @property(cc.Label) dateTime: cc.Label = null;
    @property(cc.Label) gid: cc.Label = null;
    @property(cc.Label) tid: cc.Label = null;
    @property(cc.Label) rid: cc.Label = null;
    apiList = {
        getTotalData: 'mem/web/getTotalData.php',
        getWinLossByGame: 'mem/web/getWinLossByGame.php',
        getTableData: 'mem/web/getTableData.php',
        getRoundsData: 'mem/web/getRoundsData.php',
        getOneRound: 'mem/web/getOneRound.php',
    };
    @observable type = 'getTotalData';
    @observable resData: [] = [];
    @observable roundData: any = {};
    @observable totalData: any = {};
    @observable breadcrumb: Breadcrumb = { dateTime: '' };

    init(): void {
        this.getTotalData();
        this.dateTime.getComponent(cc.Button).node.on('click', () => {
            this.getTotalData();
        });
        this.gid.getComponent(cc.Button).node.on('click', () => {
            this.getWinLossByGame(this.breadcrumb.dateTime);
        });
        this.tid.getComponent(cc.Button).node.on('click', () => {
            this.getTableData(this.formatCellData('postDate', this.breadcrumb.dateTime), this.breadcrumb.gid);
        });
        this.rid.getComponent(cc.Button).node.on('click', () => {
            this.getRoundsData(
                this.formatCellData('postDate', this.breadcrumb.dateTime),
                this.breadcrumb.gid,
                this.breadcrumb.tid,
            );
        });
    }

    public async getTotalData() {
        const { sid, _sessid } = rootStore.user;
        if (!sid && !_sessid) {
            return;
        }
        const { apiBase } = rootStore.setting;
        const { lang } = rootStore.app;

        if (lang == 'zh') {
            (lang as string) = 'tw';
        }

        const url = apiBase + this.apiList.getTotalData;
        let param = {};

        param = {
            _sessid: _sessid,
            sid: sid,
        };
        const res = await this.post(`${url}?${Date.now()}`, this._getFormData(param));

        runInAction(() => {
            this.breadcrumb = { dateTime: '', gid: undefined, tid: undefined, rid: undefined };
            this.type = 'getTotalData';
            this.resData = res.items;
            this.totalData = res.total;
        });
    }
    public async getWinLossByGame(date: string) {
        const { sid, _sessid } = rootStore.user;
        if (!sid && !_sessid) {
            return;
        }
        const { apiBase } = rootStore.setting;
        const { lang } = rootStore.app;

        if (lang == 'zh') {
            (lang as string) = 'tw';
        }

        const url = apiBase + this.apiList.getWinLossByGame;
        let param = {};

        param = {
            _sessid: _sessid,
            sid: sid,
            date: date,
        };

        const res = await this.post(`${url}?${Date.now()}`, this._getFormData(param));

        runInAction(() => {
            this.breadcrumb = { dateTime: date, gid: undefined, tid: undefined, rid: undefined };
            this.type = 'getWinLossByGame';
            this.resData = res.items;
            this.totalData = res.total;
        });
    }
    public async getTableData(date: string, gid: number) {
        const { sid, _sessid } = rootStore.user;
        if (!sid && !_sessid) {
            return;
        }
        const { apiBase } = rootStore.setting;
        const { lang } = rootStore.app;

        if (lang == 'zh') {
            (lang as string) = 'tw';
        }

        const url = apiBase + this.apiList.getTableData;
        let param = {};

        param = {
            _sessid: _sessid,
            sid: sid,
            date: date,
            gid: gid,
        };
        const res = await this.post(`${url}?${Date.now()}`, this._getFormData(param));

        runInAction(() => {
            this.breadcrumb = { dateTime: this.breadcrumb.dateTime, gid: gid, tid: undefined, rid: undefined };
            this.type = 'getTableData';
            this.resData = res.items;
            this.totalData = res.total;
        });
    }
    public async getRoundsData(date: string, gid: number, tid: number) {
        const { sid, _sessid } = rootStore.user;
        if (!sid && !_sessid) {
            return;
        }
        const { apiBase } = rootStore.setting;
        const { lang } = rootStore.app;

        if (lang == 'zh') {
            (lang as string) = 'tw';
        }

        const url = apiBase + this.apiList.getRoundsData;
        let param = {};

        param = {
            _sessid: _sessid,
            sid: sid,
            date: date,
            gid: gid,
            tid: tid,
        };

        const res = await this.post(`${url}?${Date.now()}`, this._getFormData(param));

        runInAction(() => {
            this.breadcrumb = {
                dateTime: this.breadcrumb.dateTime,
                gid: this.breadcrumb.gid,
                tid: tid,
                rid: undefined,
            };
            this.type = 'getRoundsData';
            this.resData = res.items;
            this.totalData = res.total;
        });
    }
    public async getOneRound(tid: number, rid: number) {
        const { sid, _sessid } = rootStore.user;
        if (!sid && !_sessid) {
            return;
        }
        const { apiBase } = rootStore.setting;
        const { lang } = rootStore.app;

        if (lang == 'zh') {
            (lang as string) = 'tw';
        }

        const url = apiBase + this.apiList.getOneRound;
        let param = {};

        param = {
            _sessid: _sessid,
            sid: sid,
            tid: tid,
            rid: rid,
        };

        const res = await this.post(`${url}?${Date.now()}`, this._getFormData(param));

        runInAction(() => {
            this.breadcrumb = {
                dateTime: this.breadcrumb.dateTime,
                gid: this.breadcrumb.gid,
                tid: this.breadcrumb.tid,
                rid: rid,
            };
            this.type = 'getOneRound';
            this.roundData = res.players[0].detail;
            this.resData = [];
        });
    }
    @render renderBreadcrumb(): void {
        if (this.breadcrumb.dateTime.length) {
            this.dateTime.string = this.formatCellData('datetime', this.breadcrumb.dateTime);
        } else {
            this.dateTime.string = '';
        }
        if (this.breadcrumb.gid !== undefined) {
            this.gid.string = this.formatCellData('gid', this.breadcrumb.gid) || '';
        } else {
            this.gid.string = '';
        }
        if (this.breadcrumb.tid !== undefined) {
            this.tid.string = 'Table ' + this.breadcrumb.tid?.toString() || '';
        } else {
            this.tid.string = '';
        }
        if (this.breadcrumb.rid !== undefined) {
            this.rid.string = '#' + this.breadcrumb.rid?.toString() || '';
        } else {
            this.rid.string = '';
        }
    }
    @render renderData(): void {
        if (!this.resData.length) return;
        let temp = null;

        this.headRoot.removeAllChildren();
        this.totalRoot.removeAllChildren();
        this.bodyRoot.removeAllChildren();
        this.roundRoot.removeAllChildren();
        this.resultRoot.active = false;
        this.tableView.active = true;

        switch (this.type) {
            case 'getTotalData':
                temp = this.dateData;
                break;
            case 'getWinLossByGame':
                temp = this.winLossByGame;
                break;
            case 'getTableData':
                temp = this.tableData;
                break;
            case 'getRoundsData':
                temp = this.roundsData;
                break;
        }
        const head = cc.instantiate(temp.getChildByName('headTr'));
        this.headRoot.addChild(head);
        const total = cc.instantiate(temp.getChildByName('totalTr'));
        total.getChildByName('total').getComponent(cc.Label).string = this.totalData.winloss.toString();
        this.totalRoot.addChild(total);
        for (let i = 0; i < this.resData.length; i++) {
            this.addCell(this.bodyRoot, cc.instantiate(temp.getChildByName('tr')), this.resData[i]);
        }
        this.tableScrollView.scrollToTop(0.1);
    }
    @render renderRoundData(): void {
        if (!this.roundData.balls) return;

        this.headRoot.removeAllChildren();
        this.totalRoot.removeAllChildren();
        this.bodyRoot.removeAllChildren();
        this.roundRoot.removeAllChildren();
        this.resultRoot.active = true;
        this.tableView.active = false;

        const temp = this.oneRound;
        const head = cc.instantiate(temp.getChildByName('headTr'));
        this.headRoot.addChild(head);
        for (let i = 0; i < this.roundData.bet.length; i++) {
            this.addCell(this.roundRoot, cc.instantiate(temp.getChildByName('tr')), this.roundData.bet[i]);
        }
        for (let i = 0; i < this.roundData.balls.length; i++) {
            this.addCards(this.roundData.balls[i], i);
        }
        this.resultScrollView.scrollToTop(0.1);
    }
    public addCell(rooNode: cc.Node, btnNode: cc.Node, cellData: any) {
        btnNode.children.forEach((child) => {
            if (child.name !== 'line') {
                btnNode.getChildByName(child.name).getComponent(cc.Label).string = this.formatCellData(
                    child.name,
                    cellData[child.name],
                );
            }
        });
        btnNode.getComponent(cc.Button).node.on('click', () => {
            switch (this.type) {
                case 'getTotalData':
                    this.getWinLossByGame(cellData.startDate);
                    break;
                case 'getWinLossByGame':
                    this.getTableData(cellData.date, cellData.gid);
                    break;
                case 'getTableData':
                    this.getRoundsData(this.formatCellData('postDate', cellData.datetime), cellData.gid, cellData.tid);
                    break;
                case 'getRoundsData':
                    this.getOneRound(cellData.tid, cellData.rid);
                    break;
            }
        });
        rooNode.addChild(btnNode);
    }
    public addCards(data: any, index: number) {
        const card = this.resultRoot.getComponentsInChildren(sp.Skeleton)[index];
        card.setSkin(this.toSuit(data));
    }

    public formatCellData(type: string, data: any) {
        switch (type) {
            case 'postDate':
                return lib.date.format_date(data, 'yyyy-MM-DD');
            case 'startDate':
            case 'date':
                return (
                    lib.date.getDateText(data).year +
                    ' ' +
                    lib.date.getDateText(data).month +
                    ' ' +
                    lib.date.getDateText(data).day
                );
            case 'datetime':
                return (
                    lib.date.getDateText(data).month +
                    ' ' +
                    lib.date.getDateText(data).day +
                    ' ' +
                    lib.date.getDateText(data).hour +
                    ':' +
                    lib.date.getDateText(data).min
                );
            case 'winloss':
                return Util.numberAddComma(data);
            case 'gid':
                if (data === 201) return 'Baccarat';
            default:
                return data;
        }
    }
    public post(_url: string, postData: ArionFormData): Promise<any> {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.addEventListener('load', () => {
                if (xhr.readyState == 4 && xhr.status >= 200 && xhr.status < 400) {
                    resolve(JSON.parse(xhr.responseText));
                } else if (xhr.readyState == 0 || xhr.status == 0) {
                    cc.log('do nothing');
                } else {
                    reject({ statusText: xhr.responseText });
                }
            });
            xhr.open('POST', _url, true);
            xhr.setRequestHeader('Content-Type', `multipart/form-data; boundary=${postData.getBoundaryKey()}`);
            xhr.send(postData.arrayBuffer());
        });
    }
    private _getFormData(param: unknown) {
        const body = new ArionFormData();
        for (const [key, value] of Object.entries(param)) {
            body.append(key, value as string);
        }
        return body;
    }
    public toSuit(cardId: string): string {
        return cardId;
    }
}
class ArionFormData {
    private _boundary_key = 'arionformdata';
    private _boundary = '';
    private _end_boundary = '';
    private _result = '';

    constructor() {
        this._boundary = '--' + this._boundary_key;
        this._end_boundary = this._boundary + '--';
    }

    getBoundaryKey() {
        return this._boundary_key;
    }

    append(key: string, value: string) {
        this._result += this._boundary + '\r\n';
        this._result += 'Content-Disposition: form-data; name="' + key + '"' + '\r\n\r\n';
        this._result += value + '\r\n';
    }

    arrayBuffer() {
        this._result += '\r\n' + this._end_boundary;
        const charArr = [];

        for (let i = 0; i < this._result.length; i++) {
            // 取出文本的charCode（10进制）
            charArr.push(this._result.charCodeAt(i));
        }

        const array = new Uint8Array(charArr);
        return array.buffer;
    }
}
