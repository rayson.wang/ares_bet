import { AudioController } from '../ares_base/GameLibsIndex';
import { LobbySound } from './LobbyController';
import { runInAction, observer, render, rootStore } from './LobbyIndex';
import PlayerHistoryController from './playerHistory/PlayerHistoryController';

const { ccclass, property } = cc._decorator;
export enum PopupType {
    SETTING = 'setting',
    GAMERULE = 'gameRule',
    GOODROAD = 'goodRoad',
    PLAYER_HISTORY = 'playerHistory',
}
@ccclass
@observer
export default class MenuController extends cc.Component {
    @property(cc.Node) panelRoot: cc.Node = null;
    @property(cc.Node) setting: cc.Node = null;
    @property(cc.Node) gameRule: cc.Node = null;
    @property(cc.Node) goodRoad: cc.Node = null;
    @property(cc.Node) playerHistory: cc.Node = null;
    @property(cc.Toggle) toggleMusic: cc.Toggle = null;
    @property(cc.Toggle) toggleSound: cc.Toggle = null;

    @property(PlayerHistoryController) playerHistoryController: PlayerHistoryController;
    audioController: AudioController;

    // LIFE-CYCLE CALLBACKS:

    onLoad(): void {
        this.toggleMusic.node.on('toggle', (toggle: cc.Toggle) => {
            this.audioController.play(LobbySound.click, { loop: false });
            runInAction(() => {
                rootStore.app.isMusicMute = !toggle.isChecked;
            });
        });
        this.toggleSound.node.on('toggle', (toggle: cc.Toggle) => {
            this.audioController.play(LobbySound.click, { loop: false });
            runInAction(() => {
                rootStore.app.isSoundMute = !toggle.isChecked;
            });
        });
    }
    init(audioController: AudioController): void {
        this.audioController = audioController;
    }

    @render renderMuteSetting(): void {
        this.toggleMusic.isChecked = !rootStore.app.isMusicMute;
        this.toggleSound.isChecked = !rootStore.app.isSoundMute;
    }

    @render renderPopupView(): void {
        this.panelRoot.children.forEach((n) => {
            n.active = false;
        });
        switch (rootStore.lobby.settingPopupType) {
            case 'setting':
                this.setting.active = true;
                break;
            case 'gameRule':
                this.gameRule.active = true;
                break;
            case 'goodRoad':
                this.goodRoad.active = true;
                break;
            case 'playerHistory':
                this.playerHistory.active = true;
                this.playerHistoryController.init();
                break;
        }
    }

    // update (dt) {}
}
