import GameSelectBtn from './GameSelectBtn';
import { TableSelect } from './table/TableSelect';
import MenuController from './MenuController';
import {
    runInAction,
    AresLobbyControllerBase,
    ISocketObserver,
    AresLobbySocketSubject,
    render,
    observer,
    SocketRManager,
    AresLobbyEvent,
    AresGameState,
    $enum,
    Util,
} from './LobbyIndex';
import LobbyStore from './LobbyStore';
import { AudioController } from '../ares_base/GameLibsIndex';
import { GameType, rootStore } from '../ares_base/GameBaseIndex';
import { TableStore } from '../ares_base/store/TableStore';

const { ccclass, property } = cc._decorator;

export enum LobbySound {
    bgm,
    click,
}

@ccclass
@observer
export default class LobbyController extends AresLobbyControllerBase {
    store = new LobbyStore(rootStore);
    bundle: cc.AssetManager.Bundle = null;
    @property(cc.Node) gameNodeRoot: cc.Node = null;
    @property(cc.Node) gameSelectRoot: cc.Node = null;
    @property(cc.Node) tableSelectRoot: cc.Node = null;

    @property(cc.Label) username: cc.Label = null;
    @property(cc.Label) credit: cc.Label = null;
    @property(cc.Button) home: cc.Button = null;
    @property(cc.Button) menu: cc.Button = null;

    @property(cc.Node) menuPanel: cc.Node = null;
    @property(AudioController) audioController: AudioController;
    @property(MenuController) menuController: MenuController;
    public selectBtnMap = new Map<GameType, cc.Node>();
    protected _socketObserver: ISocketObserver;
    private _prefabMap = new Map<number, cc.Prefab>();

    // LIFE-CYCLE CALLBACKS:

    onLoad(): void {
        super.onLoad && super.onLoad();
        const soundCount = $enum(LobbySound).length;
        this.audioController.init(soundCount);
        this.audioController.play(LobbySound.bgm, { loop: true });
        this.menuController.init(this.audioController);

        this._socketObserver = {
            notify: ({ event, data }) => {
                this.receive(event, data);
            },
        };

        const socket = SocketRManager.Instance;

        AresLobbySocketSubject.getInstance().addObserver(this._socketObserver);

        const menuPanelBtns = this.menuPanel.getComponentsInChildren(cc.Button);
        menuPanelBtns.forEach((btn) => {
            btn.node.on('click', () => {
                this.audioController.play(LobbySound.click, { loop: false });
                runInAction(() => {
                    this.store.settingPopupType = btn.node.name;
                });
            });
        });

        this.menu.node.on('click', () => {
            this.audioController.play(LobbySound.click, { loop: false });
        });

        this.home.node.on('click', () => {
            this.audioController.play(LobbySound.click, { loop: false });
            if (!this.audioController.isPlaying(LobbySound.bgm)) {
                this.audioController.play(LobbySound.bgm, { loop: true });
            }
            this.gameNodeMap.forEach((game) => game.removeFromParent());
            // this.audioController.play(LobbySound.bgm, { loop: true });
        });

        // debug
        (globalThis as any).lobby = this;
    }

    async init(bundle: cc.AssetManager.Bundle): Promise<LobbyStore> {
        runInAction(() => {
            this.store = new LobbyStore(rootStore);
            this.store.selectGameId = GameType.Baccarat;
            rootStore.lobby = this.store;
        });

        const {
            user: userStore,
            app: { uiType, urlSearchParams },
        } = rootStore;
        const gameList = this.store.getGameList();

        const ogid = urlSearchParams.get('ogid');

        const promiseList: Promise<any>[] = [];
        for (let index = 0; index < gameList.length; index++) {
            const gid = gameList[index];
            promiseList.push(
                new Promise<void>((resolve, reject) => {
                    cc.log(`[loadGameSelectedBtn.${gid}] prefab ${uiType}`);
                    bundle.load(`games/${gid}_select_btn_${uiType}`, cc.Prefab, (loadPrefabErr, prefab: cc.Prefab) => {
                        loadPrefabErr && reject(loadPrefabErr);
                        const selectBtn = cc.instantiate(prefab);
                        console.log('selectBtn', selectBtn);
                        this.addSelectBtn(gid, selectBtn, index);
                        resolve();
                    });
                }).catch((e) => {
                    cc.error(e);
                    // cc.director.emit(DialogEvent.SHOW_CONFIRM, {
                    //     title: 'ERROR',
                    //     content: `errMsg:${e}`,
                    //     confirmCB: (sender: DialogController) => {
                    //         sender.close();
                    //     },
                    // } as DialogParams);
                }),
            );
        }
        const comingsoon = ['202_select_btn', 'multi_table_btn'];
        comingsoon.forEach((item) => {
            promiseList.push(
                new Promise<void>((resolve, reject) => {
                    cc.log(`[loadGameSelectedBtn.${item}] prefab ${uiType}`);
                    bundle.load(`games/comingsoon/${item}_${uiType}`, cc.Prefab, (loadPrefabErr, prefab: cc.Prefab) => {
                        loadPrefabErr && reject(loadPrefabErr);
                        const selectBtn = cc.instantiate(prefab);
                        this.addSelectBtn(202, selectBtn, 98);
                        resolve();
                    });
                }).catch((e) => {
                    cc.error(e);
                    // cc.director.emit(DialogEvent.SHOW_CONFIRM, {
                    //     title: 'ERROR',
                    //     content: `errMsg:${e}`,
                    //     confirmCB: (sender: DialogController) => {
                    //         sender.close();
                    //     },
                    // } as DialogParams);
                }),
            );
        });

        this.bundle = bundle;
        const p = super.init(bundle);
        promiseList.push(p);
        await Promise.all(promiseList);

        return this.store;
    }

    onDestroy(): void {
        super.onDestroy && super.onDestroy();
        AresLobbySocketSubject.getInstance().removeObserver(this._socketObserver);
    }

    // update (dt) {}

    receive(event: string, data: any): void {
        if (event === AresLobbyEvent.GAMEINFO) {
            this.initTable(data);
        }
        if (event === AresLobbyEvent.STATECHANGE) {
            if (!data.tid) return;
            const tid = data.tid;
            const tableStore = this.store.tables.get(tid);
            if (!tableStore) return;

            if (data.state === AresGameState.SHOWDOWN) {
                tableStore.setHistory({ rid: data.rid, dieNumber: data.dieNumber });
            }
            tableStore.setData(data);
        }
    }

    public initTable(data: any) {
        new Promise<cc.Prefab>((resolve, reject) => {
            // show loading

            // get prefab from cache
            if (this._prefabMap.has(this.store.selectGameId)) {
                resolve(this._prefabMap.get(this.store.selectGameId));
                return;
            }

            // download prefab
            this.bundle.load(`table/tableContainer`, cc.Prefab, (loadPrefabErr, prefab: cc.Prefab) => {
                loadPrefabErr && reject(loadPrefabErr);
                this._prefabMap.set(this.store.selectGameId, prefab);
                resolve(prefab);
            });
        })
            .then(async (prefab: cc.Prefab) => {
                for (let i = 0; i < data.length; i++) {
                    await new Promise<void>((resolve, reject) => {
                        const selectTable = cc.instantiate(prefab);
                        const gameId = this.store.selectGameId;
                        const tableStore = new TableStore(rootStore);
                        tableStore.initData();
                        tableStore.setData(data[i]);
                        this.store.tables.set(data[i].tid, tableStore);
                        this.addTableItem(gameId, selectTable, i, tableStore, data);
                        this.scheduleOnce(() => resolve());
                    });
                }
            })
            .finally(() => {
                // close loading
            })
            .catch((e) => {
                cc.error(e);
                // cc.director.emit(DialogEvent.SHOW_CONFIRM, {
                //     title: 'ERROR',
                //     content: `errMsg:${e}`,
                //     confirmCB: (sender: DialogController) => {
                //         sender.close();
                //     },
                // } as DialogParams);
            });
    }
    public addSelectBtn(gid: GameType, btnNode: cc.Node, zIndex: number): void {
        btnNode
            .getComponent(GameSelectBtn)
            .init(gid, this.store)
            .then(() => {
                this.selectBtnMap.set(gid, btnNode);
                cc.log({ 'this.store.currentSelectedGame': this.store.currentSelectedGame });
                btnNode.zIndex = zIndex;
                btnNode.on('click', () => {
                    console.log('btn click');
                    this.audioController.play(LobbySound.click, { loop: false });
                    // this.renderCurrentSelectedGame.call(this, gid);
                });
                this.gameSelectRoot.addChild(btnNode);
            });
        // });
    }

    public addTableItem(gameId: number, btnNode: cc.Node, zIndex: number, tableStore: TableStore, data: any): void {
        btnNode.getComponent(TableSelect).init(tableStore);

        this.selectBtnMap.set(gameId, btnNode);
        btnNode.zIndex = zIndex;
        btnNode.on('click', () => {
            this.audioController.play(LobbySound.click, { loop: false });
            this.renderCurrentSelectedGame.call(this, { gid: gameId, tid: tableStore.tid });
        });
        this.tableSelectRoot.addChild(btnNode);
    }

    renderCurrentSelectedGame({ gid, tid }: { gid: any; tid: number }): void {
        const socketConnected = rootStore.app.socketConnected;

        // const gid = this.store.currentSelectedGame;
        cc.log(`[renderCurrentSelectedGame]`, socketConnected, gid);
        if (socketConnected) {
            // const btnNode = this.selectBtnMap.get(gid);
            // btnNode?.getComponent(cc.Toggle).check();
            // const btnIndex = this.gameSelectRoot.children.indexOf(btnNode);

            // const gameNode = this.gameNodeMap.get(gid);
            this.scheduleOnce(() => {
                // this.gameNodeMap.forEach((game) => game.removeFromParent());

                const task = this._downloadTaskPromises.get(gid);
                console.log({ task, down: task.isDownloading, done: task.isLoadDone });
                // if (task.isDownloading || task.isLoadDone) {
                //     const gameNode = this.gameNodeMap.get(gid);
                //     this.audioController.stop(LobbySound.bgm);
                //     gameNode && this.gameNodeRoot.addChild(gameNode);
                //     return;
                // }

                task.loadBundlePromise(tid)
                    .then(() => {
                        console.log('loadBundlePromise');
                        const gameNode = this.gameNodeMap.get(gid);
                        this.gameNodeMap.forEach((game) => game.removeFromParent());
                        this.audioController.stop(LobbySound.bgm);
                        gameNode && this.gameNodeRoot.addChild(gameNode);
                    })
                    .catch((err: any) => {
                        task.isDownloading = false;
                        task.isLoadDone = false;
                    });
            });
        }
    }
    @render renderUserinfo(): void {
        // this.username.string = 'SBO_XTctlW6T8TDSxmTBEL9QyA-3d-3d_0';
        this.username.string = rootStore.user.name;
        this.credit.string = Util.numberAddComma(rootStore.user.credit);
    }
}
