import { GameType, UIType } from '../ares_base/GameBaseIndex';
import { AresLobbyStoreBase, observable, Util } from './LobbyIndex';

export default class LobbyStore extends AresLobbyStoreBase {
    // @override
    get gameSize(): cc.Size {
        return this.root.app.uiType === UIType.MOBILE ? cc.size(1920, 1080) : cc.size(1920, 1080);
    }

    _gameList = [GameType.Baccarat];

    getGameList(): GameType[] {
        const gameList = super.getGameList();
        Util.moveElementToFirst(gameList, this.currentSelectedGame);
        return gameList;
    }

    @observable currentSelectedGame: GameType;
    @observable selectGameId: number = null;

    @observable settingPopupType: string;
}
