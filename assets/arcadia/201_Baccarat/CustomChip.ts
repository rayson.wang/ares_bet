import { observable, observer, render, runInAction, Util } from '../../shared_frameworks/SharedFrameworksIndex';

const { ccclass, property, requireComponent } = cc._decorator;

interface MinMaxConfig {
    min: number;
    max: number;
}

@ccclass
@observer
@requireComponent(cc.Button)
@requireComponent(cc.ProgressBar)
export default class CustomChip extends cc.Component {
    @property(cc.Label) labelAmount: cc.Label = null;

    private _minAmount = 0;
    private _maxAmount = 100;
    @observable private _currentAmount = 0;
    @observable private _labelAnimationObj = { v: 0 };

    // popup panel
    @property(cc.Layout) inputKeyBoard: cc.Layout = null;
    @property(cc.Label) outputNum: cc.Label = null;
    @property(cc.Button) btnClear: cc.Button = null;

    // onLoad(): void {}

    set minMax({ min, max }: MinMaxConfig) {
        max = max < min ? min + 1 : max;
        this._minAmount = min;
        this._maxAmount = max;
        runInAction(() => {
            this._currentAmount =
                this._currentAmount > max ? max : this._currentAmount < min ? min : this._currentAmount;
        });
    }

    get minMax(): MinMaxConfig {
        return {
            min: this._minAmount,
            max: this._maxAmount,
        };
    }

    set amount(amount: number) {
        runInAction(() => {
            this._currentAmount = Math.trunc(
                amount > this._maxAmount ? this._maxAmount : amount < this._minAmount ? this._minAmount : amount,
            );
        });
    }

    get amount(): number {
        return this._currentAmount;
    }

    @render renderProgressBar(): void {
        const compProgressBar = this.getComponent(cc.ProgressBar);
        const toPercentage = this._currentAmount / (this._maxAmount - this._minAmount) || 0;
        cc.tween(compProgressBar).stop().to(1, { progress: toPercentage }, { easing: cc.easing.cubicOut }).start();

        runInAction(() => {
            cc.tween(this._labelAnimationObj)
                .to(
                    0.5,
                    { v: this._currentAmount },
                    {
                        easing: cc.easing.cubicOut,
                        progress: (start: number, end: number, current: number, ratio: number) => {
                            current = start + (end - start) * ratio;
                            if (this.labelAmount) {
                                this.labelAmount.string = Util.thousandSpr(Math.floor(current)).toString();
                            }

                            return current;
                        },
                    },
                )
                .start();
        });
    }

    // start() {}

    // update (dt) {}
}
