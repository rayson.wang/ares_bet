import { AresClientEvent, AresGameState, BetChipsCount } from '../../ares_base/AresBaseIndex';
import { AudioController } from '../../ares_base/GameLibsIndex';
import {
    computed,
    observable,
    runInAction,
    $enum,
    observer,
    render,
    Util,
    reactor,
    react,
} from '../../shared_frameworks/SharedFrameworksIndex';
import { BaccaratBetType, BaccaratStore } from './BaccaratStore';
import { BaccaratSound } from './BaccaratTypes';
import BetTypeButton, { ButtonState } from './BetTypeButton';
import CustomChip from './CustomChip';

type BetData = Record<string, number>;
type BetChipsCountList = Record<string, BetChipsCount>;
export interface BetDataParams {
    tid: string;
    betData: BetData;
    betChipsCount: BetChipsCountList;
    isNoCommission: boolean;
}

export enum BetEvent {
    BET_CONFIRM = 'BetEvent_BetConfirm',
    MAX_BET_LIMIT_REACHED = 'MAX_BET_LIMIT_REACHED',
}

const { ccclass, property } = cc._decorator;

@ccclass
@observer
export default class BetPanel extends cc.Component {
    @property(cc.Layout) chipsRoot: cc.Layout = null;
    store: BaccaratStore;
    audioController: AudioController;
    @observable private _betChipsCountTemp: BetChipsCount[] = []; // the chips count temp (unconfirmed) in a times
    @observable private _amountsTemp: number[] = [];

    @observable private _allBetChipsCountTemp: BetChipsCount[] = []; // save one round the chips counts
    @observable private _allAmountsTemp: number[] = []; // save one round the amount

    @observable private _lastBetChipsCountTemp: BetChipsCount[] = [];
    @observable private _lastAmountsTemp: number[] = [];
    @computed get _oneConfirmAmountTemp(): number {
        return this._amountsTemp.reduce((sum, amount) => sum + amount, 0);
    }
    @computed get _allAmountTemp(): number {
        return this._allAmountsTemp.reduce((sum, amount) => sum + amount, 0);
    }
    @computed get _lastAmountTemp(): number {
        return this._lastAmountsTemp.reduce((sum, amount) => sum + amount, 0);
    }

    @computed get betChipsCountForRender(): BetChipsCount[] {
        const { betChipsCountArray } = this.store;
        const { _betChipsCountTemp } = this;
        return betChipsCountArray.map((chipsCountArr, betTypeIndex) => {
            return chipsCountArr.map((count, chipIndex) => (count += _betChipsCountTemp[betTypeIndex][chipIndex]));
        });
    }
    @computed({ keepAlive: true }) get chips(): number[] {
        // the chip amount of normal chips + 1 custom chip
        return [...this.store.chips, this.customChip.amount];
    }
    @computed({ keepAlive: true }) get chipNodes(): cc.Node[] {
        // the chip nodes of normal chips + 1 custom chip
        return [...this.chipsRoot.node.children, this.customChip.node];
    }
    @observable chipIndex = 0; // the index of current selected chip
    @computed get selectedChip(): cc.Node {
        // get the node of selected chip
        if (this.chipIndex !== undefined) {
            return this.chipNodes[this.chipIndex];
        }
        return null;
    }

    @observable private _isPending = false;

    // BetType PageView
    @property(cc.PageView) betTypePageView: cc.PageView = null;
    @property([BetTypeButton]) betTypeButtons: BetTypeButton[] = [];
    @property([BetTypeButton]) fixedBetTypeButtons: BetTypeButton[] = [];
    private _itemsOrgPos: cc.Vec2[] = [];

    // Turn page btn
    @property(cc.Button) btnPrevPage: cc.Button = null;
    @property(cc.Button) btnNextPage: cc.Button = null;

    // Action Buttons
    @property(cc.Button) btnRepeat: cc.Button = null;
    @property(cc.Button) btnAuto: cc.Button = null;
    @property(cc.Button) btnCancel: cc.Button = null;
    @property(cc.Button) btnConfirm: cc.Button = null;

    // Custom Chip ProgressBar
    @property(CustomChip) customChip: CustomChip = null;

    // Labels
    @property(cc.Label) labelTotalBet: cc.Label = null;

    // On Table Chip Images
    @property(cc.SpriteFrame) onTableChipImage: cc.SpriteFrame = null;

    onLoad(): void {
        // REPEAT
        this.btnRepeat.node.on('click', () => {
            cc.log('btnRepeat');
            this.audioController.play(BaccaratSound.buttonCheck);
            this._lastBetChipsCountTemp.forEach((chipsCountArr, betIndex) => {
                if (!this.betTypeButtons[betIndex].isLocked) {
                    chipsCountArr.forEach((count, coinIndex) => {
                        for (let i = 0; i < count; i++) {
                            this.betChips({
                                to: new cc.Vec3(
                                    this.betTypeButtons[betIndex].node.position.x,
                                    this.betTypeButtons[betIndex].node.position.y +
                                        this.betTypeButtons[betIndex].node.parent.y,
                                ),
                                parent: this.betTypeButtons[betIndex].node,
                                betTypeIndex: betIndex,
                                chipIndex: coinIndex,
                            });

                            this.audioController.play(BaccaratSound.chips);
                        }
                    });
                }
            });
        });

        // AUTO
        this.btnAuto.node.on('click', () => {
            this.clearTempChips();
            this.setPendingState(false);
            cc.log('btnAuto');
        });

        // CANCEL
        this.btnCancel.node.on('click', () => {
            cc.log('btnCancel');
            this.clearTempChips();
            this.audioController.play(BaccaratSound.buttonCheck);
        });

        // CONFIRM
        this.btnConfirm.node.on('click', () => {
            cc.log('btnConfirm');
            if (this._oneConfirmAmountTemp > 0) {
                const betData: BetData = {};
                const betChipsCount: BetChipsCountList = {};
                this._amountsTemp.forEach((amount, index) => {
                    if (amount > 0) {
                        betData[index] = amount;
                    }
                });

                this._betChipsCountTemp.forEach((chipsCountArr, index) => {
                    if (chipsCountArr.some((it) => it > 0)) betChipsCount[index] = chipsCountArr;
                });

                // emit bet event
                this.node.emit(BetEvent.BET_CONFIRM, {
                    tid: this.store.tid,
                    betData: betData,
                    betChipsCount: betChipsCount,
                    isNoCommission: this.store.isNoCommission,
                } as BetDataParams);
                this.setPendingState(true);
                this.audioController.play(BaccaratSound.buttonCheck);
            }
        });

        // BetType Buttons event init
        this.betTypeButtons.forEach((it, index) => {
            it.node.on('click', () => {
                if (this.store.state !== AresGameState.BET) return;
                if (!this.selectedChip) {
                    this.audioController.play(BaccaratSound.buttonUnusable);
                    return this.chipNodes.forEach((button) => {
                        const bg = button.getChildByName('bg');
                        cc.tween(bg)
                            .repeat(2, cc.tween().to(0.1, { opacity: 255 }).to(0.1, { opacity: 128 }))
                            .start();
                    });
                } else {
                    const { chipsContainer } = it;
                    this.betChips({
                        to: new cc.Vec3(it.node.position.x, chipsContainer.position.y + chipsContainer.parent.y),
                        parent: chipsContainer,
                        betTypeIndex: index,
                        chipIndex: this.chipIndex,
                    });
                    this.audioController.play(BaccaratSound.chips);
                }
            });

            it.node.on(cc.Node.EventType.TOUCH_START, () => {
                if (this.store.state !== AresGameState.BET) return;
                if (it.getComponent(cc.Button).interactable) {
                    it.playAnim(ButtonState.MOVE);
                }
            });

            it.node.on(cc.Node.EventType.TOUCH_END, () => {
                if (this.store.state !== AresGameState.BET) return;
                if (it.getComponent(cc.Button).interactable) {
                    it.playAnim(ButtonState.CLICK);
                }
            });

            it.node.on(cc.Node.EventType.TOUCH_CANCEL, () => {
                if (this.store.state !== AresGameState.BET) return;
                if (it.getComponent(cc.Button).interactable) {
                    it.playAnim(ButtonState.IDLE);
                }
            });
        });

        // BetType PageView event init
        this._itemsOrgPos = this.betTypeButtons.map((it) => it.node.getPosition());
        this.betTypePageView.node.on('scrolling', (comp: cc.PageView) => {
            const offsetX = comp.getScrollOffset().x;
            const pageW = comp.node.width;
            const half = pageW / 2;
            const effectValue = Math.abs((half - Math.abs(offsetX % pageW)) / half);

            this.betTypeButtons.forEach((item) => {
                const index = this.betTypeButtons.indexOf(item);
                const orgPos = this._itemsOrgPos[index];
                if (this.fixedBetTypeButtons.includes(item)) {
                    // item.x = orgPos.x - offsetX; // fixed position
                    // item.y = orgPos.y + (1 - effectValue) * 50;
                    // item.opacity = 255 - 200 * cc.easing.cubicOut(1 - effectValue);
                } else {
                    // const opacity = 255 * cc.easing.cubicOut(effectValue);
                    // item.opacity = opacity;
                    // item.x = orgPos.x + 100 * cc.easing.cubicOut(1 - effectValue);
                    // item.node.angle = 360 * effectValue;
                }
            });
        });

        this.betTypePageView.node.on('page-turning', (comp: cc.PageView) => {
            const pageIndex = comp.getCurrentPageIndex();
            const currentPage = comp.getPages()[pageIndex];
            this.fixedBetTypeButtons.forEach((it) => (it.node.parent = currentPage.getChildByName('btns')));
            this.btnPrevPage.node.active = pageIndex > 0;
            this.btnNextPage.node.active = pageIndex < comp.getPages().length - 1;
            this.audioController.play(BaccaratSound.switchPage);
        });

        this.btnPrevPage.node.on('click', () => {
            this.betTypePageView.setCurrentPageIndex(this.betTypePageView.getCurrentPageIndex() - 1);
            // this.audioController.play(BaccaratSound.buttonCheck);
        });

        this.btnNextPage.node.on('click', () => {
            this.betTypePageView.setCurrentPageIndex(this.betTypePageView.getCurrentPageIndex() + 1);
            // this.audioController.play(BaccaratSound.buttonCheck);
        });

        this.btnPrevPage.node.active = false;
    }

    init({ store, audioController }: { store: BaccaratStore; audioController: AudioController }): void {
        this.store = store;
        this.audioController = audioController;

        // init temp amounts Array
        this._amountsTemp = new Array($enum(BaccaratBetType).length).fill(0);
        this._lastAmountsTemp = new Array($enum(BaccaratBetType).length).fill(0);
        this._allAmountsTemp = new Array($enum(BaccaratBetType).length).fill(0);

        // init temp chips count Array
        const length = this.chipNodes.length;
        this._betChipsCountTemp = new Array($enum(BaccaratBetType).length).fill(0).map(() => {
            return new Array(length).fill(0);
        });
        this._lastBetChipsCountTemp = new Array($enum(BaccaratBetType).length).fill(0).map(() => {
            return new Array(length).fill(0);
        });
        this._allBetChipsCountTemp = new Array($enum(BaccaratBetType).length).fill(0).map(() => {
            return new Array(length).fill(0);
        });

        // init chip click event
        this.chipNodes.forEach((chipBtn, index) => {
            chipBtn.on('click', () => {
                this.audioController.play(BaccaratSound.chipChoose);
                runInAction(() => {
                    this.chipIndex = this.chipIndex === index ? null : index;
                });
            });
        });

        // set default
        runInAction(() => (this.chipIndex = 0));
    }

    setBetData(): void {
        this._amountsTemp.forEach((amount, index) => {
            if (amount > 0) {
                this._allAmountsTemp[index] += amount;
            }
        });

        this._betChipsCountTemp.forEach((chipsCountArr, index) => {
            chipsCountArr.map((count, chipIndex) => (this._allBetChipsCountTemp[index][chipIndex] += count));
        });
    }
    setLastBetData(): void {
        // check now round do bet.

        const length = this.chipNodes.length;
        if (this._allAmountTemp > 0) {
            this._lastAmountsTemp = new Array($enum(BaccaratBetType).length).fill(0);
            this._lastBetChipsCountTemp = new Array($enum(BaccaratBetType).length).fill(0).map(() => {
                return new Array(length).fill(0);
            });
            this._allAmountsTemp.forEach((amount, index) => {
                if (amount > 0) {
                    this._lastAmountsTemp[index] = amount;
                }
            });

            this._allBetChipsCountTemp.forEach((chipsCountArr, index) => {
                if (chipsCountArr.some((it) => it > 0)) this._lastBetChipsCountTemp[index] = chipsCountArr;
            });
        }

        // clear now round amount.
        this._allAmountsTemp = new Array($enum(BaccaratBetType).length).fill(0);
        this._allBetChipsCountTemp = new Array($enum(BaccaratBetType).length).fill(0).map(() => {
            return new Array(length).fill(0);
        });
    }

    @reactor updateChips() {
        return react(
            () => {
                return this.store.chips;
            },
            (chips) => {
                this.chipNodes.forEach((chipBtn, index) => {
                    if (!(chipBtn instanceof CustomChip)) {
                        chipBtn.getComponentInChildren(cc.Label).string = Util.simplifyNumber(chips[index]);
                        chipBtn.getComponentInChildren(cc.Label).node.color = chipBtn.getComponentInChildren(
                            cc.Sprite,
                        ).node.color;
                    }
                });
            },
        );
    }

    clearTempChips(): void {
        runInAction(() => {
            this._amountsTemp.fill(0);
            this._betChipsCountTemp.fill(new Array(this.chipNodes.length).fill(0));
        });
    }

    clear(): void {
        this.clearTempChips();
        this.betTypeButtons.forEach((it, index) => {
            it.chipsContainer.removeAllChildren();
        });
    }

    setPendingState(isPending: boolean): void {
        runInAction(() => (this._isPending = isPending));
    }

    // throw animation
    betChips({
        to,
        parent,
        betTypeIndex,
        chipIndex,
    }: {
        to: cc.Vec3;
        parent: cc.Node;
        betTypeIndex: number;
        chipIndex: number;
    }): void {
        const index = chipIndex;
        const selectedChip = this.chipNodes[index];
        if (!selectedChip || !this._betChipsCountTemp[betTypeIndex]) {
            return;
        }

        runInAction(() => {
            this._betChipsCountTemp[betTypeIndex][index]++;
            this._amountsTemp[betTypeIndex] += this.chips[index];
        });
        this.scheduleOnce(() => {
            // fly animation
            const random = Math.random() * 25;
            const position = new cc.Vec3(random + to.x, random + to.y);
            const node = cc.instantiate(selectedChip);
            const chipIndex = index;

            if (this.chipNodes[chipIndex] === this.customChip.node) {
                node.removeComponent(CustomChip);
            }
            node.targetOff(node);
            node.setPosition(
                new cc.Vec3(
                    selectedChip.position.x + selectedChip.parent.position.x + this.node.position.x,
                    this.node.position.y,
                ),
            );
            node.scale = this.node.scale;
            const angle = Util.getRandomIntElseZero(-360);
            this.node.parent.addChild(node);

            cc.tween(node)
                .to(0.25, { position, scale: 0.3, angle, opacity: 0 }, { easing: cc.easing.cubicOut })
                .call(() => node.destroy())
                .start();
        });
    }

    @render renderSelectedChip(): void {
        this.chipNodes.forEach((button) => {
            const bg = button.getChildByName('bg');
            if (button === this.selectedChip) {
                cc.tween(button).to(0.1, { scale: 1.2 }, { easing: cc.easing.circOut }).start();
                cc.tween(bg)
                    .stop()
                    .set({ opacity: 255 })
                    .to(0.25, { angle: -90 }, { easing: cc.easing.circOut })
                    .call(() => {
                        cc.tween(bg).repeatForever(cc.rotateBy(1, 20)).start();
                    })
                    .start();
            } else {
                cc.tween(button).to(0.1, { scale: 0.95 }, { easing: cc.easing.circOut }).start();
                cc.Tween.stopAllByTarget(bg);
                cc.tween(bg)
                    .set({ angle: Math.abs(bg.angle % 360) })
                    .set({ opacity: 128 })
                    .to(0.2, { angle: 0 }, { easing: cc.easing.circOut })
                    .start();
            }
        });
    }

    @render renderBtnConfirm(): void {
        const isBetTime = this.store.state === AresGameState.BET;
        this.btnConfirm.interactable = isBetTime && !this._isPending && this._oneConfirmAmountTemp > 0;
        // btn bg
        this.btnConfirm.target.color = this.btnCancel.interactable ? cc.color(110, 243, 190) : cc.color(110, 243, 190);
        // btn txt
        this.btnConfirm.target.getComponentInChildren(cc.Sprite).node.color = this.btnConfirm.interactable
            ? cc.color(110, 243, 190)
            : cc.color(255, 255, 255);
    }

    @render renderBtnCancel(): void {
        const isBetTime = this.store.state === AresGameState.BET;
        this.btnCancel.interactable = isBetTime && !this._isPending && this._oneConfirmAmountTemp > 0;
        // btn bg
        this.btnCancel.target.color = this.btnCancel.interactable ? cc.color(219, 47, 47) : cc.color(110, 243, 190);
        // btn txt
        this.btnCancel.target.getComponentInChildren(cc.Sprite).node.color = this.btnCancel.interactable
            ? cc.color(219, 47, 47)
            : cc.color(255, 255, 255);
    }

    @render renderBtnAuto(): void {
        const isBetTime = this.store.state === AresGameState.BET;
        this.btnAuto.interactable = isBetTime;
        // btn bg
        this.btnAuto.target.color = this.btnCancel.interactable ? cc.color(110, 243, 190) : cc.color(110, 243, 190);
        // btn txt
        this.btnAuto.target.getComponentInChildren(cc.Sprite).node.color = this.btnAuto.interactable
            ? cc.color(110, 243, 190)
            : cc.color(255, 255, 255);
    }

    @render renderBtnRepeat(): void {
        const isBetTime = this.store.state === AresGameState.BET;
        this.btnRepeat.interactable =
            isBetTime && this._lastAmountTemp > 0 && this._allAmountTemp <= 0 && this._oneConfirmAmountTemp <= 0;
        // btn bg
        this.btnRepeat.target.color = this.btnRepeat.interactable ? cc.color(110, 243, 190) : cc.color(110, 243, 190);
        // btn txt
        this.btnRepeat.target.getComponentInChildren(cc.Sprite).node.color = this.btnRepeat.interactable
            ? cc.color(110, 243, 190)
            : cc.color(255, 255, 255);
    }

    @render renderLabelTotalBet(): void {
        this.labelTotalBet.string = Util.thousandSpr(this.store.totals.reduce((sum, v) => sum + v)) || '0';
    }

    @render renderBetAmountTemp(): void {
        this.betTypeButtons.forEach((betTypeBtn, index) => {
            const amount = this._amountsTemp[index];
            betTypeBtn.tempAmountLabel.node.parent.active = this.store.state === AresGameState.BET && amount > 0;
            betTypeBtn.tempAmountLabel.string = Util.thousandSpr(amount);
        });
    }

    @render renderOdds(): void {
        const { oddsStringByState } = this.store;
        this.betTypeButtons.forEach((betTypeBtn, index) => {
            const oddsLabel = betTypeBtn.odds;
            if (oddsLabel && oddsStringByState) {
                oddsLabel.string = oddsStringByState[index];
            }
        });
    }

    @render renderTotals(): void {
        const { totals, state, totalsResult } = this.store;
        this.betTypeButtons.forEach((betType, index) => {
            const value = state === AresGameState.SHOWDOWN ? totalsResult?.[index] : totals?.[index];
            let valueStr = Util.thousandSpr(value || 0);
            if (state === AresGameState.SHOWDOWN && value > 0) {
                valueStr = `+${valueStr}`;
            }
            betType.total.string = valueStr;
            betType.total.node.active = totals[index] > 0 || value !== 0;
        });
    }

    @render renderTableChips(): void {
        const { betChipsCountForRender } = this;
        this.betTypeButtons.forEach((btn: BetTypeButton, betTypeIndex) => {
            const chipsContainer = btn.chipsContainer;
            chipsContainer.removeAllChildren();
            const chipsCountArr = betChipsCountForRender[betTypeIndex];

            chipsCountArr.forEach((count, index) => {
                for (let i = 0; i < count; i++) {
                    if (i >= 15) break;
                    const chipNode = new cc.Node();
                    chipNode.addComponent(cc.Sprite).spriteFrame = this.onTableChipImage;
                    chipNode.parent = chipsContainer;
                    const bg = this.chipNodes[index].getChildByName('bg');
                    const color = bg ? bg.color : cc.color(255, 255, 255);
                    chipNode.color = color;
                    const toX = (index / this.chips.length) * chipsContainer.width - chipsContainer.width / 2;
                    const random = Util.getRandomBySeed(this.store.rid + betTypeIndex + i);
                    const offsetX = toX - 5 + random * 5;
                    const y = -chipsContainer.height / 2 + i * 8;
                    chipNode.setPosition(new cc.Vec3(offsetX, y));
                }
            });
        });
    }
}
