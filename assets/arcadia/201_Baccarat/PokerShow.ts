import { AresGameState } from '../../ares_base/AresBaseIndex';
import { rootStore, UIType } from '../../ares_base/GameBaseIndex';
import { AudioController, TweenHandler } from '../../ares_base/GameLibsIndex';
import { observer, render } from '../../shared_frameworks/SharedFrameworksIndex';
import { BaccaratBetType, BaccaratStore } from './BaccaratStore';
import { BaccaratSound, CardTween } from './BaccaratTypes';

export enum CardFrameState {
    IDLE = 'idle',
    PLAYER_WIN = 'p_win',
    PLAYER_WIN_2 = 'p_win2',
    BANKER_WIN = 'b_win',
    BANKER_WIN_2 = 'b_win2',
}

const { ccclass, property } = cc._decorator;

@ccclass
@observer
export default class PokerShow extends cc.Component {
    @property(cc.Prefab) pokerCard: cc.Prefab = null;
    @property(cc.Node) shoeCards: cc.Node = null;
    @property(cc.Node) player: cc.Node = null;
    @property(cc.Node) banker: cc.Node = null;
    @property(cc.Label) playerPoints: cc.Label = null;
    @property(cc.Label) bankerPoints: cc.Label = null;
    @property(sp.Skeleton) spineCardFrame: sp.Skeleton = null;
    @property(cc.Node) drawCards: cc.Node = null;
    dealPosition: cc.Vec3[] = [];
    drawPosition: cc.Vec3[] = [];
    deck: cc.Vec3 = new cc.Vec3(-390, 190);
    shoe: cc.Vec3 = new cc.Vec3(530, 175);
    store: BaccaratStore = null;
    dealCardDuration = 0.35;
    pokerScale = 0.25;
    tweenHandler: TweenHandler;
    audioController: AudioController;
    onLoad(): void {
        cc.view.setResizeCallback(this.resizePokerCamera.bind(this));
        this.dealPosition.push(new cc.Vec3(-148, 150));
        this.dealPosition.push(new cc.Vec3(90, 150));
        this.dealPosition.push(new cc.Vec3(-90, 150));
        this.dealPosition.push(new cc.Vec3(148, 150));
        this.dealPosition.push(new cc.Vec3(-225, 150));
        this.dealPosition.push(new cc.Vec3(225, 150));

        this.drawPosition.push(new cc.Vec3(345, 150));
        this.drawPosition.push(new cc.Vec3(400, 150));

        this.playerPoints.node.setPosition(new cc.Vec3(-35, 145));
        this.bankerPoints.node.setPosition(new cc.Vec3(35, 145));
    }

    init({
        store,
        tweenHandler,
        audioController,
    }: {
        store: BaccaratStore;
        tweenHandler: TweenHandler;
        audioController: AudioController;
    }): void {
        this.store = store;
        this.tweenHandler = tweenHandler;
        this.audioController = audioController;
    }

    showdown(): void {
        const dealCardCount = 4;
        const dieNumber = this.store.dieNumber || [];
        console.log(...dieNumber);
        const openPlayerCardsDelay = 0;
        this.scheduleOnce(() => {
            this.player.getComponentsInChildren(sp.Skeleton).forEach((spine, i) => {
                const index = i * 2;
                const skin = dieNumber[index];
                console.log({ skin });
                if (skin) {
                    // skin = 0 為空牌
                    spine.setSkin(skin.toString());
                }

                const aniEntry = spine.setAnimation(0, 'play_1', false);
                spine.setTrackCompleteListener(aniEntry, () => {
                    const dieNumbers = this.store.dieNumber.slice(0, index + 1);
                    const pokerPoints = this.store.toPokerPoints(dieNumbers);
                    this.playerPoints.string = pokerPoints[0].toString();
                });
                this.audioController.play(BaccaratSound.flipCard);
            });
        }, openPlayerCardsDelay);

        const openBankerCardsDelay = openPlayerCardsDelay + 1.1 * 1;
        this.scheduleOnce(() => {
            this.banker.getComponentsInChildren(sp.Skeleton).forEach((spine, j) => {
                const index = j * 2 + 1;
                const skin = dieNumber[index];
                console.log({ skin });
                if (skin) {
                    // skin = 0 為空牌
                    spine.setSkin(skin.toString());
                }
                const aniEntry = spine.setAnimation(0, 'play_1', false);
                spine.setTrackCompleteListener(aniEntry, () => {
                    const dieNumbers = this.store.dieNumber.slice(0, index + 1);
                    const pokerPoints = this.store.toPokerPoints(dieNumbers);
                    this.bankerPoints.string = pokerPoints[1].toString();
                    this.audioController.play(BaccaratSound.flipCard);
                });
                this.audioController.play(BaccaratSound.flipCard);
            });
        }, openBankerCardsDelay);

        const drawCardDealDelay = openBankerCardsDelay + 1.1 * 1;

        this.scheduleOnce(() => {
            let drawCardIndex = 0;
            for (let index = dealCardCount; index < dieNumber.length; index++) {
                const skin = dieNumber[index];
                const card = this.drawCards.getComponentsInChildren(sp.Skeleton)[drawCardIndex];
                if (skin && card) {
                    const position = this.dealPosition[index];
                    const angle = 90;
                    const delay = index === dealCardCount ? 0 : 0.35 + 1.1;
                    const spine = card.getComponent(sp.Skeleton);
                    if (skin) {
                        // skin = 0 為空牌
                        spine.setSkin(skin.toString());
                    }
                    cc.tween(card.node)
                        .delay(delay)
                        .to(0.35, { position, angle }, { easing: cc.easing.circOut })
                        .call(() => {
                            const aniEntry = spine.setAnimation(0, 'play_2', false);
                            spine.setTrackCompleteListener(aniEntry, () => {
                                const dieNumbers = this.store.dieNumber.slice(0, index + 1);
                                const pokerPoints = this.store.toPokerPoints(dieNumbers);
                                this.playerPoints.string = pokerPoints[0].toString();
                                this.bankerPoints.string = pokerPoints[1].toString();
                            });
                            card.node.parent = index % 2 === 0 ? this.player : this.banker;
                            this.audioController.play(BaccaratSound.flipCard);
                        })
                        .start();
                    drawCardIndex += 1;
                }
            }
        }, drawCardDealDelay);

        // play frame animation
        const { currentWinTypeIndexes, currentShowdownTime } = this.store;
        if (currentWinTypeIndexes.includes(BaccaratBetType.player)) {
            if (this.store.dieNumber[4] !== 0) {
                this.setFrameAnime({
                    aniName: CardFrameState.PLAYER_WIN_2,
                    loop: true,
                    currentTime: currentShowdownTime,
                });
            } else {
                this.setFrameAnime({
                    aniName: CardFrameState.PLAYER_WIN,
                    loop: true,
                    currentTime: currentShowdownTime,
                });
            }
        } else if (currentWinTypeIndexes.includes(BaccaratBetType.banker)) {
            if (this.store.dieNumber[4] !== 0) {
                this.setFrameAnime({
                    aniName: CardFrameState.BANKER_WIN_2,
                    loop: true,
                    currentTime: currentShowdownTime,
                });
            } else {
                this.setFrameAnime({
                    aniName: CardFrameState.BANKER_WIN,
                    loop: true,
                    currentTime: currentShowdownTime,
                });
            }
        }
    }

    cardToDeck(holder: cc.Node, shoe: cc.Node): void {
        const totalCards = [
            ...this.player.getComponentsInChildren(sp.Skeleton),
            ...this.banker.getComponentsInChildren(sp.Skeleton),
        ];
        const delayTime = 0.05;
        const cardboxTime = 1;
        // 牌盒動畫
        shoe.color = new cc.Color(130, 130, 130, 130);
        shoe.opacity = 130;
        holder.color = new cc.Color(255, 255, 255, 255);
        holder.opacity = 255;

        // 收牌動畫
        totalCards.forEach((card, i) => {
            const delay = i * delayTime + cardboxTime;
            const angle = -30;
            const position = this.deck;
            const scale = this.pokerScale - 0.05;
            cc.tween(card.node)
                .delay(delay)
                .to(0.35, { position, angle, scale }, { easing: cc.easing.circOut })
                .call(() => {
                    card.node.active = false;
                    card.node.parent = null;
                })
                .start();
        });

        this.audioController.play(BaccaratSound.dealCards2);
    }

    dealCardsWithIndex(from: number): void {
        for (let i = from; i < 6; i++) {
            const card = cc.instantiate(this.pokerCard);
            card.setPosition(this.shoe);
            card.scale = i === from ? this.pokerScale - 0.1 : 0;
            card.angle = 45;
            this.shoeCards.addChild(card);
        }
        for (let i = from; i < 6; i++) {
            let position: cc.Vec3;
            let parent: cc.Node;
            if (i < 4) {
                position = this.dealPosition[i];
                parent = i % 2 === 0 ? this.player : this.banker;
            } else {
                position = this.drawPosition[i - 4];
                parent = this.drawCards;
            }
            const cardIndex = i === from ? 0 : i - from;
            const nextCard = cardIndex + 1;
            const cards = this.shoeCards.getComponentsInChildren(sp.Skeleton).map((spine) => spine.node);
            nextCard < 6 - from &&
                this.scheduleOnce(() => (cards[nextCard].scale = this.pokerScale), 0.3 + 0.35 * cardIndex);
            const initialDelay = this.dealCardDuration * cardIndex;
            const { delay, currentTime } = this.tweenHandler.getDelayAndCurrentTime(initialDelay, 0);
            console.log('bet current time ', delay, currentTime);
            this.dealCardTween({
                node: cards[cardIndex],
                position,
                angle: 0,
                delay,
                scale: this.pokerScale,
                currentTime,
                callback: () => {
                    cards[cardIndex].parent = parent;
                },
            });
        }
    }

    dealCardTween({ node, delay, position, angle, currentTime, scale, callback }: CardTween): void {
        const easing = cc.easing.circOut;
        const duration = this.dealCardDuration;
        console.log('currentTime', currentTime);
        this.tweenHandler.setTween({
            tween: cc
                .tween(node)
                .delay(delay)
                .to(duration, { position, angle, scale }, { easing })
                .call(callback)
                .start(),
            currentTime,
        });
        this.scheduleOnce(() => {
            this.audioController.play(BaccaratSound.dealCards1);
        }, delay);
    }

    nextRound(holder: cc.Node, shoe: cc.Node): void {
        if (this.player.childrenCount > 0 || this.banker.childrenCount > 0)
            // 已經發過牌 不須再發
            return;

        // 牌盒動畫
        shoe.color = new cc.Color(255, 255, 255);
        shoe.opacity = 255;
        holder.color = new cc.Color(130, 130, 130);
        holder.opacity = 130;

        const drawCards = this.drawCards.getComponentsInChildren(sp.Skeleton);
        const hasDrawCard = this.drawCards.getComponentsInChildren(sp.Skeleton).length;
        this.playerPoints.string = '0';
        this.bankerPoints.string = '0';
        if (hasDrawCard) {
            drawCards.forEach((card, i) => {
                const position = this.dealPosition[i];
                const parent = i % 2 === 0 ? this.player : this.banker;
                this.dealCardTween({
                    node: card.node,
                    position,
                    scale: this.pokerScale,
                    angle: 0,
                    currentTime: 0,
                    delay: this.dealCardDuration * i,
                    callback: () => {
                        card.node.parent = parent;
                    },
                });
            });
        }

        const dealCardDelay = hasDrawCard * this.dealCardDuration;
        this.scheduleOnce(() => {
            const from = Math.max(hasDrawCard, hasDrawCard - 1);
            this.dealCardsWithIndex(from);
        }, dealCardDelay);

        this.scheduleOnce(() => {
            shoe.color = new cc.Color(130, 130, 130);
            shoe.opacity = 130;
        }, 6 * this.dealCardDuration);
    }

    setFrameAnime({
        aniName,
        currentTime = 0,
        loop = false,
    }: {
        aniName: CardFrameState;
        currentTime?: number;
        loop?: boolean;
    }): void {
        this.spineCardFrame.setAnimation(0, aniName, loop);
        (this.spineCardFrame as any).update(currentTime);
    }

    @render showPokerPoints(): void {
        if (this.store.state === AresGameState.ANIMATE) {
            this.playerPoints.node.active = true;
            this.bankerPoints.node.active = true;
        } else {
            this.playerPoints.node.active = false;
            this.bankerPoints.node.active = false;
        }
    }

    resizePokerCamera(): void {
        console.log('aa raha hai');
        const widthRatio = cc.game.canvas.width / cc.game.canvas.height;
        const heightRatio = cc.game.canvas.height / cc.game.canvas.width;
        const designRatio = cc.Canvas.instance.designResolution.width / cc.Canvas.instance.designResolution.height;
        if (widthRatio > designRatio) {
            cc.warn('constant');
            const fov = rootStore.app.uiType === UIType.MOBILE ? 54 : 65;
            this.node.getComponentInChildren(cc.Camera).fov = fov;
        } else {
            if (cc.game.canvas.height > cc.game.canvas.width) {
                cc.warn('height');
                const fov = rootStore.app.uiType === UIType.MOBILE ? 90 : 60;
                const fovFactor = rootStore.app.uiType === UIType.MOBILE ? 18 : 24;
                const ratio = heightRatio > 3 ? 3 + (heightRatio - 3) / 4 : heightRatio;
                this.node.getComponentInChildren(cc.Camera).fov = fov + ratio * fovFactor;
            } else {
                cc.warn('width');
                const fov = rootStore.app.uiType === UIType.MOBILE ? 31 : 70;
                const fovFactor = rootStore.app.uiType === UIType.MOBILE ? 60 : 0;
                this.node.getComponentInChildren(cc.Camera).fov = fov + heightRatio * fovFactor;
            }
        }
    }
}
