import { AresClientEvent, AresGameEvent, AresGameState, TableControllerBase } from '../../ares_base/AresBaseIndex';
import { DialogController, DialogParams, GameType, rootStore } from '../../ares_base/GameBaseIndex';
import { TweenHandler } from '../../ares_base/GameLibsIndex';
import BaccaratRoadMapController from '../../components/BaccaratRoadMap/BaccaratRoadMapController';
import BaccaratRoadMapUIController from '../../components/BaccaratRoadMap/BaccaratRoadMapUIController ';
import {
    $enum,
    observer,
    react,
    reactor,
    render,
    runInAction,
    SocketRManager,
    Util,
} from '../../shared_frameworks/SharedFrameworksIndex';
import { BaccaratBetType, BaccaratGameState, BaccaratStore } from './BaccaratStore';
import BaccaratBetPanel, { BetDataParams, BetEvent } from './BaccaratBetPanel';
import PokerShow, { CardFrameState } from './PokerShow';
import { BaccaratSound } from './BaccaratTypes';
import CustomChipPanel from './CustomChipPanel';
import { ButtonState } from './BetTypeButton';
import BetRangePanel from './prefabs/BetRangePanel/BetRangePanel';
import BetTotalPanel from './prefabs/TotalBetPanel/TotalBetPanel';

const { ccclass, property } = cc._decorator;

@ccclass
@observer
export default class BaccaratGameController extends TableControllerBase {
    store: BaccaratStore = null;
    pokerShow: PokerShow = null;
    @property(cc.Node) baccaratTable: cc.Node = null;
    @property(cc.Node) result: cc.Node = null;
    @property(cc.Prefab) tablePrefab: cc.Prefab = null;
    @property(TweenHandler) tweenHandler: TweenHandler = null;
    @property(cc.Node) shoe: cc.Node = null;
    @property(cc.Node) holder: cc.Node = null;
    @property(cc.Node) roadMapRoot: cc.Node = null;
    @property(cc.Prefab) roadMapTemplate: cc.Prefab = null;
    @property(cc.Prefab) roadMapUIPrefab: cc.Prefab = null;
    roadMap: BaccaratRoadMapController = null;

    roadMapUI: BaccaratRoadMapUIController = null;
    @property(BaccaratBetPanel) baccaratBetPanel: BaccaratBetPanel = null;

    @property(cc.Sprite) bankerOddsTip: cc.Sprite = null;

    // Toggle No Commission
    @property(cc.Node) toggleNoCommissionRoot: cc.Node = null;
    @property(cc.Toggle) toggleNoCommission: cc.Toggle = null;

    // spine new_shoe
    @property(sp.Skeleton) spineNewShoe: sp.Skeleton = null;
    private _shoePosition: cc.Vec3[] = [new cc.Vec3(0, 0, 0), new cc.Vec3(0, 0, 0)];

    // bettingArea
    @property(cc.Node) bettingArea: cc.Node = null;

    // custom chip panel
    @property(CustomChipPanel) customChipPanel: CustomChipPanel = null;

    // Positions
    @property(cc.Node) positionDealer: cc.Node = null;
    @property(cc.Node) positionPlayer: cc.Node = null;
    // main range
    @property(cc.Label) mainRange: cc.Label = null;

    // winlose result
    @property(cc.Label) winloseAmount: cc.Label = null;

    // BetLimit Panel
    @property(BetRangePanel) betRangePanel: BetRangePanel = null;
    // BetTotal Panel
    @property(BetTotalPanel) betTotalPanel: BetTotalPanel = null;

    onLoad(): void {
        super.onLoad && super.onLoad();
        // console.log('[baccarat]');
        const soundCount = $enum(BaccaratSound).length;
        this.audioController.init(soundCount);

        this.roadMap = cc.instantiate(this.roadMapTemplate).getComponent(BaccaratRoadMapController);
        this.roadMapUI = null;
        this.roadMapRoot.addChild(this.roadMap.node);

        this.baccaratBetPanel.node.on(BetEvent.BET_CONFIRM, (betDataPack: BetDataParams) => {
            SocketRManager.Instance.emit(AresClientEvent.BET, betDataPack);
        });
        // this.betPanel.on(BetEvent.MAX_BET_LIMIT_REACHED, this.showMaxBetLimitMessage.bind(this), this);

        // No Commission toggle event init
        this.toggleNoCommission.node.on('toggle', (toggle: cc.Toggle) => {
            this.audioController.play(BaccaratSound.buttonCheck);
            runInAction(() => {
                this.store.isNoCommission = Boolean(toggle.isChecked);
            });
        });

        // Play BGM
        this.audioController.play(BaccaratSound.BGM, { loop: true });

        this._shoePosition[0] = this.shoe.position;
        this._shoePosition[1] = new cc.Vec3(cc.Vec3.ZERO);
    }

    async init(gid: GameType, tid: number, bundle: cc.AssetManager.Bundle): Promise<BaccaratStore> {
        super.init && super.init(gid, tid, bundle);
        this.store = new BaccaratStore(rootStore);
        // load Localization
        await new Promise<void>((resolve, reject) => {
            bundle.load('Localization', cc.JsonAsset, (loadLocalizationErr, asset: cc.JsonAsset) => {
                loadLocalizationErr && reject(loadLocalizationErr);
                runInAction(() => {
                    for (const lang of rootStore.app.langList) {
                        Object.assign(rootStore.setting.localization[lang], asset.json[lang]);
                    }
                });
                cc.log('[loadLocalization]', rootStore.setting.localization);
                resolve();
            });
        });
        const baccaratTable = cc.instantiate(this.tablePrefab);
        this.baccaratTable.addChild(baccaratTable);
        this.pokerShow = this.baccaratTable.getComponentInChildren(PokerShow);
        this.pokerShow.init({
            store: this.store,
            tweenHandler: this.tweenHandler,
            audioController: this.audioController,
        });
        this.baccaratBetPanel.init({ store: this.store, audioController: this.audioController });
        this.customChipPanel.init({ audioController: this.audioController });
        this.betRangePanel.init({ store: this.store });
        this.betTotalPanel.init({ store: this.store });

        return this.store;
    }

    receive(event: string, data: any): void {
        super.receive && super.receive(event, data);
        // console.log('event', event);
        // console.log('data', data);

        const { state } = data;

        // REBUILD_INFO
        if (event === AresGameEvent.TABLE_INFO) {
            this.baccaratBetPanel.setPendingState(false);
        }

        // STATE_CHANGE
        if (event === AresGameEvent.STATE_CHANGE) {
            if (state === AresGameState.BET) {
                this.baccaratBetPanel.clearTempChips();
                this.baccaratBetPanel.setPendingState(false);
            }
        }

        // BET_RES
        if (event === AresGameEvent.BET_RES) {
            const { code, msg } = data;
            if (code === 0) {
                cc.log('bet success');
                this.audioController.play(BaccaratSound.betConfirm);

                this.baccaratBetPanel.setBetData();
            } else {
                cc.log('bet failed');
                const langData = this.store.root.setting.currentLangData;
                this.gameDialog.getComponent(DialogController).showConfirm({
                    title: `bet failed`,
                    content: msg,
                    confirmCB: (sender: DialogController) => {
                        sender.close();
                    },
                } as DialogParams);
            }
            this.baccaratBetPanel.clearTempChips();
            this.baccaratBetPanel.setPendingState(false);
        }
    }

    renderStateOnNextTick(): void {
        super.renderStateOnNextTick();

        // BET
        if (this.store.state === AresGameState.BET) {
            const { currentBetTime } = this.store;
            this.pokerShow.nextRound(this.holder, this.shoe);
            this.baccaratBetPanel.betTypeButtons.forEach((btn) => btn.playAnim(ButtonState.IDLE));
            this.renderBetTypeButtons();

            // start bet animation
            const { enterEntry } = this.playStartBetAnime({
                enter: 0,
                idle: 0.3,
                out: 0.7,
            });
            this.startBetAnime.setTrackStartListener(enterEntry, () => {
                this.audioController.play(BaccaratSound.placeYourBet);
            });
            (this.startBetAnime as any).update(currentBetTime);
        } else {
            this.baccaratBetPanel.clearTempChips();
            this.baccaratBetPanel.betTypeButtons.forEach((btn) => {
                btn.getComponent(cc.Button).interactable = false;
            });
        }

        // LOCK
        if (this.store.state === AresGameState.LOCK) {
            const { currentLockTime, totals } = this.store;
            this.audioController.play(BaccaratSound.noMoreBet, { currentTime: currentLockTime });
        }

        // ANIMATE
        if (this.store.state === AresGameState.ANIMATE) {
            this.pokerShow.showdown();
        } else {
        }

        // SHOWDOWN
        if (this.store.state === AresGameState.SHOWDOWN) {
            const { currentShowdownTime, currentWinTypeIndexes, totalsResult, totals } = this.store;
            this.pokerShow.cardToDeck(this.holder, this.shoe);

            // play win / lose animation
            const animeDuration = 1.5;
            this.baccaratBetPanel.betTypeButtons.forEach((btn, index) => {
                const childrenCount = btn.chipsContainer.childrenCount;
                const isWin = totals[index] && totalsResult[index] >= 0;
                const isWinLight = currentWinTypeIndexes.includes(index);
                if (btn.isLocked) {
                    btn.playAnim(ButtonState.IDLE);
                } else {
                    btn.playAnim(isWinLight ? ButtonState.WIN : ButtonState.IDLE, true);
                }

                // chip fly
                btn.chipsContainer.children.reverse().forEach((chipNode, i) => {
                    const toPosition = chipNode.convertToNodeSpaceAR(
                        (isWin ? this.positionPlayer : this.positionDealer).convertToWorldSpaceAR(cc.Vec3.ZERO),
                    );
                    const delay = i * (animeDuration / childrenCount);
                    this.tweenHandler.setTween({
                        tween: cc
                            .tween(chipNode)
                            .delay(delay)
                            .to(animeDuration - delay, { position: toPosition }, { easing: cc.easing.quintInOut })
                            .to(animeDuration - delay, { opacity: 128 }),
                        currentTime: currentShowdownTime,
                    });
                });
            });

            // winlose amount animation
            if (this.store.totals.some((it) => it > 0)) {
                const result = totalsResult.reduce((acc, it) => acc + it);
                const resultStr = Util.thousandSpr(Util.roundDownFixed(result, 2));
                this.winloseAmount.string = result >= 0 ? `+${resultStr}` : resultStr;
                this.winloseAmount.node.color = result >= 0 ? cc.color(0, 255, 51) : cc.color(255, 255, 255);
                this.winloseAmount.node.active = true;
                this.winloseAmount.getComponent(cc.Animation).play(null, currentShowdownTime);
            }
            this.baccaratBetPanel.setLastBetData();
        } else {
            this.pokerShow.setFrameAnime({ aniName: CardFrameState.IDLE });
            this.winloseAmount.node.active = false;
        }

        // NEW_SHOE
        if (this.store.state === BaccaratGameState.NEW_SHOE) {
            const { currentNewShoeTime } = this.store;
            cc.log('[currentNewShoeTime]', currentNewShoeTime);
            this.bettingArea.runAction(cc.fadeOut(0.5));
            this.tweenHandler.setTween({
                tween: cc
                    .tween(this.spineNewShoe.node)
                    .to(0.5, { position: this._shoePosition[1] })
                    .delay(5)
                    .to(0.5, { position: this._shoePosition[0] }),
                currentTime: currentNewShoeTime,
            });
            this.spineNewShoe.node.color = new cc.Color(255, 255, 255);
            this.spineNewShoe.node.opacity = 255;
            this.holder.color = new cc.Color(130, 130, 130);
            this.holder.opacity = 130;
            const aniEntry = this.spineNewShoe.setAnimation(0, 'play', false);
            this.spineNewShoe.setTrackCompleteListener(aniEntry, () => {
                this.spineNewShoe.node.color = new cc.Color(130, 130, 130, 130);
                this.spineNewShoe.node.opacity = 130;
            });

            (this.spineNewShoe as any).update(currentNewShoeTime);
            this.audioController.play(BaccaratSound.newShoe, { currentTime: currentNewShoeTime });
        } else {
            this.spineNewShoe.setAnimation(0, 'stop', false);
            this.bettingArea.runAction(cc.fadeIn(0.5));
        }
    }

    clearResult(): void {
        this.result.getComponent(cc.Label).string = '';
    }

    // click event: open roadMap UI
    onOpenRoadMapUIBtnClick(): void {
        console.log('onOpenRoadMapUIBtnClick');

        if (!this.roadMapUI || !this.roadMapUI.isValid) {
            this.roadMapUI = cc.instantiate(this.roadMapUIPrefab).getComponent(BaccaratRoadMapUIController);
            this.roadMapUI.node.parent = this.node;
            this.roadMapUI.init({ store: this.store });
        }
    }

    @render renderRoadMap(): void {
        cc.log('renderRoadMap: ', this.store.history.length);
        if (!this.roadMap) return;
        runInAction(() => {
            this.roadMap.history = this.store.history?.map(({ dieNumber }) => dieNumber);
        });
    }

    @render renderBankerOddsTip(): void {
        this.bankerOddsTip.node.active = this.store.isNoCommission;
    }

    @render renderIsNoCommissionToggle(): void {
        const { isNoCommission, totals } = this.store;
        this.toggleNoCommission.isChecked = isNoCommission;

        if (this.store.state === AresGameState.BET) {
            this.toggleNoCommissionRoot.active = !(
                this.baccaratBetPanel._oneConfirmAmountTemp > 0 || totals.some((v) => v !== 0)
            );
        } else {
            this.toggleNoCommissionRoot.active = false;
        }
    }

    // @override
    @render renderTableID(): void {
        if (this.tableID) {
            this.tableID.string = `TABLE ID: ${this.store.tid}`;
        }
    }

    // @override
    @render renderRoundID(): void {
        this.roundID.string = `ROUND ID: #${this.store.rid ?? ''}`;
    }

    // @override
    @render renderRoundCount(): void {
        this.roundCount.string = `ROUNDS: #${this.store.roundCount ?? ''}`;
    }

    @render renderMainRange(): void {
        const [min, max] = this.store.mainRange;
        this.mainRange.string = `${min} - ${max}`;
    }

    @render renderBetTypeButtons(): void {
        const { isLuckySixLocked, isBigSmallLocked, isPairsLocked } = this.store;

        this.baccaratBetPanel.betTypeButtons.forEach((btn, index) => {
            if (index === BaccaratBetType.luckySix) {
                // lucky6
                if (isLuckySixLocked) {
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.luckySix].lock();
                } else {
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.luckySix].unlock();
                }
            } else if (index === BaccaratBetType.big || index === BaccaratBetType.small) {
                // bigSmall
                if (isBigSmallLocked) {
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.big].lock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.small].lock();
                } else {
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.big].unlock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.small].unlock();
                }
            } else if (
                [
                    BaccaratBetType.eitherPair,
                    BaccaratBetType.perfectPair,
                    BaccaratBetType.playerPair,
                    BaccaratBetType.bankerPair,
                ].includes(index)
            ) {
                if (isPairsLocked) {
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.eitherPair].lock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.perfectPair].lock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.playerPair].lock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.bankerPair].lock();
                } else {
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.eitherPair].unlock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.perfectPair].unlock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.playerPair].unlock();
                    this.baccaratBetPanel.betTypeButtons[BaccaratBetType.bankerPair].unlock();
                }
            } else {
                btn.unlock();
            }

            // if (this.store.state === AresGameState.LOCK) {
            //     if (totals[index] > 0) {
            //         btn.playAnim(ButtonState.BET);
            //     } else {
            //         btn.playAnim(ButtonState.IDLE);
            //     }
            // }
        });
    }

    @reactor updateCustomChipMinMax() {
        return react(
            () => {
                return this.store.mainRange;
            },
            (range) => {
                // set custom chip minMax value
                const [min, max] = range;
                this.baccaratBetPanel.customChip.minMax = {
                    min: min,
                    max: max,
                };
                this.baccaratBetPanel.customChip.amount = max / 2;
            },
        );
    }
}
