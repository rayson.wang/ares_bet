import { AresGameState, AresGameStoreBase } from '../../ares_base/AresBaseIndex';
import { RootStore } from '../../ares_base/GameBaseIndex';
import {
    $enum,
    computed,
    observable,
    reaction,
    runInAction,
    Util,
} from '../../shared_frameworks/SharedFrameworksIndex';

export enum BaccaratBetType {
    player = 0,
    banker = 1,
    tie = 2,
    luckySix = 3,
    eitherPair = 4,
    perfectPair = 5,
    playerPair = 6,
    bankerPair = 7,
    big = 8,
    small = 9,
    playerDragonBonus = 10,
    bankerDragonBonus = 11,
    playerOdd = 12,
    playerEven = 13,
    bankerOdd = 14,
    bankerEven = 15,
}

export enum BaccaratGameState {
    NEW_SHOE = 'NEW_SHOE',
}
export type BaccaratDieNumbersType = Array<number>;

type BaccaratBetTypeKey = keyof typeof BaccaratBetType;

export class BaccaratStore extends AresGameStoreBase<BaccaratDieNumbersType, BaccaratBetTypeKey> {
    // @override
    constructor(root: RootStore) {
        super(root);
        reaction(
            () => this.currentTime,
            () => {
                if (
                    this.isNewShoe &&
                    this.state === AresGameState.SHOWDOWN &&
                    this.nextStateTime - this.currentTime <= 4000 // go to "NEW_SHOE" state after 4000ms
                ) {
                    runInAction(() => {
                        this.isNewShoe = false;
                        this.state = BaccaratGameState.NEW_SHOE;
                        this.history = [];
                    });
                }
            },
        );
    }
    readonly betTypeKeys = $enum(BaccaratBetType).getKeys();

    @computed get bankerDieNumber(): BaccaratDieNumbersType {
        if (!this.dieNumber) {
            return [0, 0, 0];
        }
        return [0, 2, 4].map((index) => (index < this.dieNumber.length ? this.dieNumber[index] : 0));
    }

    @computed get playerDieNumber(): BaccaratDieNumbersType {
        if (!this.dieNumber) {
            return [0, 0, 0];
        }
        return [1, 3, 5].map((index) => (index < this.dieNumber.length ? this.dieNumber[index] : 0));
    }

    toPokerPoints(dieNumber: BaccaratDieNumbersType): BaccaratDieNumbersType {
        if (!dieNumber) {
            return [0, 0];
        }
        return [this.toPlayerPoint(dieNumber), this.toBankerPoint(dieNumber)];
    }

    @computed get bankerPoint(): number {
        if (!this.dieNumber) {
            return 0;
        }
        return this.toBankerPoint(this.dieNumber);
    }

    toPlayerPoint(dieNumber: BaccaratDieNumbersType): number {
        if (!dieNumber || !dieNumber.length) {
            return 0;
        }
        return this.toPokerPoint(this.toPlayerDieNumbers(dieNumber));
    }

    toPlayerDieNumbers(dieNumber: BaccaratDieNumbersType): BaccaratDieNumbersType {
        return [0, 2, 4].map((index) => (index < dieNumber.length ? dieNumber[index] : 0));
    }

    toBankerPoint(dieNumber: BaccaratDieNumbersType): number {
        if (!dieNumber || !dieNumber.length) {
            return 0;
        }
        return this.toPokerPoint(this.toBankerDieNumbers(dieNumber));
    }

    toBankerDieNumbers(dieNumber: BaccaratDieNumbersType): BaccaratDieNumbersType {
        return [1, 3, 5].map((index) => (index < dieNumber.length ? dieNumber[index] : 0));
    }

    toPokerPoint(dieNumber: BaccaratDieNumbersType): number {
        const totalPoint = dieNumber.reduce((sum, point) => {
            const originPoint = this.toOriginPoint(point);
            return sum + originPoint;
        }, 0);
        return totalPoint % 10;
    }

    toOriginPoint(cardId: number): number {
        const point = !cardId ? 0 : cardId % 13 || 13;
        return point >= 10 ? 0 : point;
    }

    toSuit(cardId: number): number {
        if ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13].includes(cardId)) {
            return 1;
        } else if ([14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26].includes(cardId)) {
            return 2;
        } else if ([27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39].includes(cardId)) {
            return 3;
        } else if ([40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52].includes(cardId)) {
            return 4;
        }
    }

    isPair(dieNumber: BaccaratDieNumbersType): boolean {
        return dieNumber.length < 2 ? false : dieNumber[0] !== 0 && dieNumber[0] % 13 === dieNumber[1] % 13;
    }

    isPerfectPair(dieNumber: BaccaratDieNumbersType): boolean {
        return dieNumber.length < 2
            ? false
            : dieNumber[0] !== 0 &&
                  this.toOriginPoint(dieNumber[0]) === this.toOriginPoint(dieNumber[1]) &&
                  this.toSuit(dieNumber[0]) === this.toSuit(dieNumber[1]);
    }

    newShoeTime = 4000;
    isNewShoe = false;
    get currentNewShoeTime(): number {
        if (this.state !== BaccaratGameState.NEW_SHOE || this.newShoeTime < this.stateCountdownMilliSec) {
            return 0;
        }
        return (this.newShoeTime - this.stateCountdownMilliSec) / 1000;
    }

    @observable isNoCommission = true;

    @computed get oddsStringByState(): string[] {
        const { oddsList, isNoCommission } = this;

        if (oddsList) {
            return oddsList.map((oddsArr, index) => {
                let str = '1 : ';
                switch (index) {
                    case BaccaratBetType.luckySix: {
                        str += `${oddsArr[0]}    /    1 : ${oddsArr[1]}`;
                        break;
                    }
                    case BaccaratBetType.banker: {
                        str += isNoCommission ? oddsArr[0] : oddsArr[1];
                        break;
                    }
                    default:
                        str += oddsArr[0];
                }
                return str;
            });
        } else {
            return null;
        }
    }

    @computed get isLuckySixLocked(): boolean {
        return !this.isNoCommission;
    }

    @computed get isBigSmallLocked(): boolean {
        return this.roundCount > 30;
    }

    @computed get isPairsLocked(): boolean {
        return this.roundCount > 60;
    }

    @computed get isPlayerNatural(): boolean {
        if (!this.dieNumber[4]) {
            const point = this.toPokerPoint([this.dieNumber[0], this.dieNumber[2]]);
            return point === 8 || point === 9;
        }
        return false;
    }

    @computed get isBankerNatural(): boolean {
        if (!this.dieNumber[5]) {
            const point = this.toPokerPoint([this.dieNumber[1], this.dieNumber[3]]);
            return point === 8 || point === 9;
        }
        return false;
    }

    // @override: win at lucky6, pays 1:0.5 on Banker
    @computed get finalOdds(): number[] {
        if (this.outcome && Object.keys(this.outcome).length) {
            return this.betTypeKeys.map((key, betTypeIndex) => {
                let odds = 0;
                if (this.outcome[betTypeIndex]) {
                    odds = Util.toFixed(this.outcome[betTypeIndex] - 1, 2);
                } else {
                    odds = this.oddsList[betTypeIndex][0];
                }
                if (betTypeIndex === BaccaratBetType.banker) {
                    if (this.isNoCommission) {
                        if (this.outcome[BaccaratBetType.luckySix]) {
                            odds = 0.5;
                        } else {
                            odds = this.oddsList[BaccaratBetType.banker][0];
                        }
                    } else {
                        odds = this.oddsList[BaccaratBetType.banker][1];
                    }
                }
                return odds;
            });
        }
        return null;
    }

    // @override
    @computed get totalsResult(): number[] {
        const currentWinTypeIndexes = this.currentWinTypeIndexes;
        const isTieWin = currentWinTypeIndexes.includes(BaccaratBetType.tie);
        const results =
            this.totals?.map((total, index) => {
                if (this.dieNumber === null || this.dieNumber === undefined) {
                    return 0;
                }
                if (isTieWin) {
                    if ([BaccaratBetType.player, BaccaratBetType.banker].includes(index)) {
                        return 0;
                    }
                    if (index === BaccaratBetType.playerDragonBonus && this.isPlayerNatural) {
                        return 0;
                    }
                    if (index === BaccaratBetType.bankerDragonBonus && this.isBankerNatural) {
                        return 0;
                    }
                }
                if (currentWinTypeIndexes.includes(index) && this.finalOdds[index]) {
                    return total * this.finalOdds[index];
                }
                return total === 0 ? 0 : total * -1;
            }) || [];
        return results;
    }
}
