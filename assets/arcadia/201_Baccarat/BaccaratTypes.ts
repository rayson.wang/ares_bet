export interface CardTween {
    node: cc.Node;
    scale: number;
    delay: number;
    position: cc.Vec3;
    angle: number;
    currentTime: number;
    callback: () => void;
}

export enum BaccaratSound {
    betConfirm,
    BGM,
    buttonCheck,
    buttonUnusable,
    chipChoose,
    chipFly,
    chips,
    dealCards1,
    dealCards2,
    flipCard,
    newShoe,
    noMoreBet,
    switchPage,
    placeYourBet,
}
