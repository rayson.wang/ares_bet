import { AudioController } from '../../ares_base/GameLibsIndex';
import { observable, observer, react, reactor, runInAction, Util } from '../../shared_frameworks/SharedFrameworksIndex';
import { BaccaratSound } from './BaccaratTypes';
import CustomChip from './CustomChip';

const { ccclass, property, requireComponent } = cc._decorator;

const amountList = [10, 100, 1000, 10000, 100000, 1000000];

@ccclass
@observer
@requireComponent(cc.Button)
@requireComponent(cc.ProgressBar)
export default class CustomChipPanel extends cc.Component {
    audioController: AudioController;
    @observable private _inputValue = 0;

    // custom chip
    @property(CustomChip) customChip: CustomChip = null;

    // popup panel
    @property(cc.Label) outputNum: cc.Label = null;
    @property(cc.Layout) buttonsRoot: cc.Layout = null;
    @property(cc.Prefab) inputButtonTemplate: cc.Prefab = null;
    @property(cc.Button) btnClear: cc.Button = null;
    @property(cc.Button) btnCancle: cc.Button = null;
    @property(cc.Button) btnSave: cc.Button = null;

    onLoad(): void {
        // popup
        this.buttonsRoot.node.removeAllChildren();

        amountList.forEach((value) => {
            const btn = cc.instantiate(this.inputButtonTemplate).getComponent(cc.Button);
            btn.target.getComponentInChildren(cc.Label).string = `+${Util.simplifyNumber(value)}`;
            btn.node.on('click', () => {
                const current = this._inputValue;
                runInAction(() => {
                    if (current + value > this.customChip.minMax.max) {
                        this._inputValue = this.customChip.minMax.max;
                    } else if (current + value < this.customChip.minMax.min) {
                        this._inputValue = this.customChip.minMax.min;
                    } else {
                        this._inputValue += value;
                    }
                });
                this.audioController.play(BaccaratSound.buttonCheck);
            });
            this.buttonsRoot.node.addChild(btn.node);
        });

        this.btnClear.node.on('click', () => {
            runInAction(() => (this._inputValue = this.customChip.minMax.min));
            this.audioController.play(BaccaratSound.buttonCheck);
        });

        this.btnSave.node.on('click', () => {
            runInAction(() => (this.customChip.amount = this._inputValue));
            this.audioController.play(BaccaratSound.buttonCheck);
        });

        this.btnCancle.node.on('click', () => {
            this.audioController.play(BaccaratSound.buttonCheck);
        });
    }

    onEnable(): void {
        super.onEnable?.();
        this._inputValue = this.customChip.amount;
    }

    init({ audioController }: { audioController: AudioController }): void {
        this.audioController = audioController;
    }

    @reactor reactTableList() {
        return react(
            () => {
                return this._inputValue;
            },
            (value) => {
                this.outputNum.string = Util.thousandSpr(value);
            },
        );
    }

    // start() {}

    // update (dt) {}
}
