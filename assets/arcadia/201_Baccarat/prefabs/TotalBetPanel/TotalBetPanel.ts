import { AresGameStoreBase } from '../../../../ares_base/AresBaseIndex';
import { GameType } from '../../../../ares_base/GameBaseIndex';
import { $enum, observer, render, Util } from '../../../../shared_frameworks/SharedFrameworksIndex';
import { BaccaratBetType } from '../../BaccaratStore';
import TotalBetRow from './TotalBetRow';

const { ccclass, property } = cc._decorator;

export interface IBetTotalInfo {
    name: string;
    amount: number;
}

@ccclass
@observer
export default class BetTotalPanel extends cc.Component {
    store: AresGameStoreBase;
    @property(cc.ScrollView) scrollView: cc.ScrollView = null;
    @property(cc.Prefab) rowPrefab: cc.Prefab = null;
    @property(cc.Label) total: cc.Label = null;

    init({ store }: { store: AresGameStoreBase }): void {
        this.store = store;
    }

    @render renderTotalBet(): void {
        const { totals, gid } = this.store;
        const langData = this.store.root.setting.currentLangData;
        const gameKey = $enum(GameType).getKeyOrThrow(gid);

        const betInfos: IBetTotalInfo[] = totals.map((amount, index) => {
            const betTypeKey = $enum(BaccaratBetType).getKeyOrThrow(index);
            return {
                // name: betTypeKey.toUpperCase(),
                name: (langData[`${gameKey}BetType_${betTypeKey}`] || betTypeKey).toUpperCase(),
                amount: amount,
            };
        });

        this.scrollView.content.removeAllChildren();

        betInfos.forEach((info) => {
            const row = cc.instantiate(this.rowPrefab).getComponent(TotalBetRow);
            row.setInfo(info);
            this.scrollView.content.addChild(row.node);
        });

        this.scrollView.scrollToTop(0);

        const total = totals.reduce((acc, it) => acc + it);
        this.total.string = Util.numberAddComma(total);
        this.total.node.color = total > 0 ? cc.color(255, 255, 255) : cc.color(128, 128, 128);
    }
}
