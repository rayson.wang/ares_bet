import { Util } from '../../../../shared_frameworks/SharedFrameworksIndex';
import { IBetTotalInfo } from './TotalBetPanel';

const { ccclass, property } = cc._decorator;

@ccclass
export default class BetRangeRow extends cc.Component {
    @property(cc.Label) itemName: cc.Label = null;
    @property(cc.Label) amount: cc.Label = null;

    setInfo(betLimitInfo: IBetTotalInfo): void {
        this.itemName.string = betLimitInfo.name;
        this.amount.string = Util.numberAddComma(betLimitInfo.amount);
        this.amount.node.color = betLimitInfo.amount > 0 ? cc.color(255, 255, 255) : cc.color(128, 128, 128);
    }
}
