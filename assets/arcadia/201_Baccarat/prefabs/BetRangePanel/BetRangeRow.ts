import { Util } from '../../../../shared_frameworks/SharedFrameworksIndex';
import { IBetRangeInfo } from './BetRangePanel';

const { ccclass, property } = cc._decorator;

@ccclass
export default class BetRangeRow extends cc.Component {
    @property(cc.Label) itemName: cc.Label = null;
    @property(cc.Label) min: cc.Label = null;
    @property(cc.Label) max: cc.Label = null;

    setInfo(betLimitInfo: IBetRangeInfo): void {
        this.itemName.string = betLimitInfo.name;
        this.min.string = Util.numberAddComma(betLimitInfo.min);
        this.max.string = Util.numberAddComma(betLimitInfo.max);
    }
}
