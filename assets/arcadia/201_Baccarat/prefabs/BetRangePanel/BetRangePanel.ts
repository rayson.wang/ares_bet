import { AresGameStoreBase } from '../../../../ares_base/AresBaseIndex';
import { GameType } from '../../../../ares_base/GameBaseIndex';
import { $enum, observer, render } from '../../../../shared_frameworks/SharedFrameworksIndex';
import { BaccaratBetType } from '../../BaccaratStore';
import BetRangeRow from './BetRangeRow';

const { ccclass, property } = cc._decorator;

export interface IBetRangeInfo {
    name: string;
    min: number;
    max: number;
}

@ccclass
@observer
export default class BetRangePanel extends cc.Component {
    store: AresGameStoreBase;
    @property(cc.ScrollView) scrollView: cc.ScrollView = null;
    @property(cc.Prefab) rowPrefab: cc.Prefab = null;

    init({ store }: { store: AresGameStoreBase }): void {
        this.store = store;
    }

    @render renderInfos(): void {
        const { betRangeArray, gid } = this.store;
        const langData = this.store.root.setting.currentLangData;
        const gameKey = $enum(GameType).getKeyOrThrow(gid);

        const betLimitInfos: IBetRangeInfo[] = betRangeArray.map((item, index) => {
            const betTypeKey = $enum(BaccaratBetType).getKeyOrThrow(index);
            return {
                // name: betTypeKey.toUpperCase(),
                name: (langData[`${gameKey}BetType_${betTypeKey}`] || betTypeKey).toUpperCase(),
                min: item[0],
                max: item[1],
            };
        });

        this.scrollView.content.removeAllChildren();

        betLimitInfos.forEach((info) => {
            const row = cc.instantiate(this.rowPrefab).getComponent(BetRangeRow);
            row.setInfo(info);
            this.scrollView.content.addChild(row.node);
        });

        this.scrollView.scrollToTop(0);
    }
}
