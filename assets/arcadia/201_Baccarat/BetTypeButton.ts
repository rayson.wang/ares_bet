import { observer, render } from '../../shared_frameworks/SharedFrameworksIndex';

export enum ButtonState {
    BET = 'bet',
    CLICK = 'click',
    IDLE = 'idle',
    MOVE = 'move',
    WIN = 'win',
}

export enum LockState {
    IDLE = 'idle',
    LOCK = 'lock',
    UNLOCK = 'unlock',
    EMPTY = 'empty',
}

const { ccclass, property, requireComponent } = cc._decorator;

@ccclass
@requireComponent(cc.Button)
// @requireComponent(cc.Sprite)
@requireComponent(sp.Skeleton)
@observer
export default class BetTypeButton extends cc.Component {
    @property(sp.Skeleton) spine: sp.Skeleton = null;
    @property(sp.Skeleton) spineLock: sp.Skeleton = null;
    @property(cc.Label) total: cc.Label = null;
    @property(cc.Label) odds: cc.Label = null;
    @property(cc.Node) chipsContainer: cc.Node = null;
    @property(cc.Label) tempAmountLabel: cc.Label = null;

    onload(): void {
        this.scheduleOnce(() => {
            this.spine.setAnimation(0, ButtonState.IDLE, true);
        });
    }

    lock(): void {
        this.getComponent(cc.Button).interactable = false;
        if (
            this.spineLock &&
            this.spineLock.animation !== LockState.LOCK &&
            this.spineLock.animation !== LockState.IDLE
        ) {
            this.spineLock.setAnimation(0, LockState.LOCK, false);
            // const aniEntry = this.spineLock.setAnimation(0, LockState.LOCK, false);
            // this.spineLock.setTrackCompleteListener(aniEntry, () => {
            //     this.spineLock.setAnimation(0, LockState.IDLE, false);
            // });
        }
    }

    unlock(): void {
        this.getComponent(cc.Button).interactable = true;
        if (
            this.spineLock &&
            this.spineLock.animation !== LockState.UNLOCK &&
            this.spineLock.animation !== LockState.EMPTY
        ) {
            const aniEntry = this.spineLock.setAnimation(0, LockState.UNLOCK, false);
            this.spineLock.setTrackCompleteListener(aniEntry, () => {
                this.spineLock.setAnimation(0, LockState.EMPTY, false);
            });
        }
    }

    get isLocked(): boolean {
        if (this.spineLock) {
            return this.spineLock.animation === LockState.LOCK && this.getComponent(cc.Button).interactable === false;
        }
        return false;
    }

    playAnim(state: ButtonState, loop = false): void {
        this.spine.setAnimation(0, state, loop);
    }
}
