import {
    $enum,
    computed,
    observable,
    observer,
    reaction,
    render,
    runInAction,
    toJS,
} from '../../shared_frameworks/SharedFrameworksIndex';

import BaccaratRoadMapController from '../../components/BaccaratRoadMap/BaccaratRoadMapController';
import { BaccaratStore } from '../../arcadia/201_Baccarat/BaccaratStore';

const { ccclass, property } = cc._decorator;

@ccclass
@observer
export default class BaccaratRoadMapUIController extends cc.Component {
    @property(cc.Label) countDown: cc.Label = null;
    @property(cc.Label) roundCount: cc.Label = null;

    @property(cc.Label) totalCounts: cc.Label[] = [];
    @property(cc.Label) probabilitys: cc.Label[] = [];
    @property(cc.Button) btnAskRoadPlayer: cc.Button = null;
    @property(cc.Button) btnAskRoadBanker: cc.Button = null;
    @property(cc.Node) roadMapRoot: cc.Node = null;
    @property(cc.Prefab) roadMapTemplate: cc.Prefab = null;
    @observable.ref store: BaccaratStore = null;
    roadMap: BaccaratRoadMapController = null;

    @observable private _isAsking = false;

    onLoad(): void {
        this.roadMap = cc.instantiate(this.roadMapTemplate).getComponent(BaccaratRoadMapController);
        this.roadMapRoot.addChild(this.roadMap.node);

        this.registerEvent();
    }

    init({ store }: { store: BaccaratStore }): void {
        runInAction(() => {
            this.store = store;
            this.roadMap.history = this.store.history?.map(({ dieNumber }) => dieNumber);
        });
    }

    registerEvent(): void {
        // ask road Player
        this.btnAskRoadPlayer.node.on('click', () => {
            const askData = [2, 1, 3, 2, 0, 0];
            runInAction(() => {
                this._isAsking = true;
                this.btnAskRoadPlayer.interactable = !this._isAsking;
                this.btnAskRoadBanker.interactable = !this._isAsking;
                this.roadMap.history.push(askData);
                this.scheduleOnce(() => {
                    this._doAskRoadFlashAni(() => {
                        runInAction(() => {
                            this.roadMap.history = this.roadMap.history.filter((el) => toJS(el) !== askData);
                            this._isAsking = false;
                            this.btnAskRoadPlayer.interactable = !this._isAsking;
                            this.btnAskRoadBanker.interactable = !this._isAsking;
                        });
                    });
                });
            });
        });

        // ask road Banker
        this.btnAskRoadBanker.node.on('click', () => {
            const askData = [1, 2, 2, 3, 0, 0];
            runInAction(() => {
                this._isAsking = true;
                this.btnAskRoadPlayer.interactable = !this._isAsking;
                this.btnAskRoadBanker.interactable = !this._isAsking;
                this.roadMap.history.push(askData);
                this.scheduleOnce(() => {
                    this._doAskRoadFlashAni(() => {
                        runInAction(() => {
                            this.roadMap.history = this.roadMap.history.filter((el) => toJS(el) !== askData);
                            this._isAsking = false;
                            this.btnAskRoadPlayer.interactable = !this._isAsking;
                            this.btnAskRoadBanker.interactable = !this._isAsking;
                        });
                    });
                });
            });
        });
    }

    @render renderInfo(): void {
        if (!this.store) return;
        cc.log('renderInfo: ', this.store.state);
        const { roundCount, betCountdownSecInt } = this.store;

        this.roundCount.string = roundCount.toString();

        this.countDown.string = !betCountdownSecInt ? '--' : betCountdownSecInt.toString();
        this.countDown.node.color = !betCountdownSecInt ? cc.color(255, 0, 0) : cc.color(255, 255, 255);
    }

    @render renderRoadMap(): void {
        if (!this.store) return;
        cc.log('renderRoadMap: ', this.store.history.length);
        runInAction(() => {
            this.roadMap.history = this.store.history?.map(({ dieNumber }) => dieNumber);
        });
    }

    @render renderTotal(): void {
        const { total } = this.roadMap;
        if (total) {
            this.totalCounts[0].string = total[1].toString(); // banker
            this.totalCounts[1].string = total[0].toString(); // player
            this.totalCounts[2].string = total[2].toString(); // tie
            this.totalCounts[3].string = total[4].toString(); // b pair
            this.totalCounts[4].string = total[3].toString(); // p pair
        }
    }

    @render renderProbabilitys(): void {
        const { total } = this.roadMap;
        if (total) {
            const allCount = total[0] + total[1] + total[2];
            this.probabilitys[0].string = ((total[1] / allCount) * 100).toFixed(2) + '%'; // banker
            this.probabilitys[1].string = ((total[0] / allCount) * 100).toFixed(2) + '%'; // player
            this.probabilitys[2].string = ((total[2] / allCount) * 100).toFixed(2) + '%'; // tie
            this.probabilitys[3].string = ((total[4] / allCount) * 100).toFixed(2) + '%'; // b pair
            this.probabilitys[4].string = ((total[3] / allCount) * 100).toFixed(2) + '%'; // p pair
        }
    }

    private _doAskRoadFlashAni(callback: () => void): void {
        const flashTween = (node: cc.Node): cc.Tween => {
            return cc
                .tween(node)
                .stop()
                .delay(0.5)
                .repeat(3, cc.tween().to(0.2, { opacity: 0 }).to(0.2, { opacity: 255 }))
                .to(0.3, { opacity: 0 })
                .call(callback)
                .start();
        };
        const bead = this.roadMap.lastBeadResultNode;
        const big = this.roadMap.lastBigResultNode;
        const bigEye = this.roadMap.lastBigEyeResultNode;
        const small = this.roadMap.lastSmallResultNode;
        const cock = this.roadMap.lastCockroachResultNode;

        bead && flashTween(bead);
        big && flashTween(big);
        bigEye && flashTween(bigEye);
        small && flashTween(small);
        cock && flashTween(cock);
    }

    onCloseClickBtn(): void {
        this.node.destroy();
    }
}
