import { observable, runInAction, toJS } from 'mobx';
import { observer, render } from '../../../shared_frameworks/SharedFrameworksIndex';
import BaccaratRoadMap from '../BaccaratRoadMapController';

const { ccclass, property } = cc._decorator;

@ccclass
@observer
export default class Demo extends cc.Component {
    // roadMapCtrl
    @property(BaccaratRoadMap) roadMapCtrl: BaccaratRoadMap = null;

    // buttons
    @property(cc.Button) btnPlayer: cc.Button = null;
    @property(cc.Button) btnBanker: cc.Button = null;
    @property(cc.Button) btnTie: cc.Button = null;

    @property(cc.Toggle) togglePlayerPair: cc.Toggle = null;
    @property(cc.Toggle) toggleBankerPair: cc.Toggle = null;

    @property(cc.Button) btnAskRoadPlayer: cc.Button = null;
    @property(cc.Button) btnAskRoadBanker: cc.Button = null;

    @property(cc.Button) btnClear: cc.Button = null;
    @property(cc.Button) btnRedo: cc.Button = null;

    // total counter
    @property(cc.Label) totalPlayer: cc.Label = null;
    @property(cc.Label) totalBanker: cc.Label = null;
    @property(cc.Label) totalTie: cc.Label = null;

    @observable private _isAsking = false;

    onLoad(): void {
        // Player
        this.btnPlayer.node.on('click', () => {
            const dieNumber = [2, 1, 3, 2, 0, 0];
            if (this.togglePlayerPair.isChecked) {
                dieNumber[0] = 2;
                dieNumber[2] = 2;
            }
            if (this.toggleBankerPair.isChecked) {
                dieNumber[1] = 1;
                dieNumber[3] = 1;
            }
            runInAction(() => this.roadMapCtrl.history.push(dieNumber));
            // this.togglePlayerPair.isChecked = this.toggleBankerPair.isChecked = false;
        });

        // Banker
        this.btnBanker.node.on('click', () => {
            const dieNumber = [1, 2, 2, 3, 0, 0];
            if (this.togglePlayerPair.isChecked) {
                dieNumber[0] = 1;
                dieNumber[2] = 1;
            }
            if (this.toggleBankerPair.isChecked) {
                dieNumber[1] = 2;
                dieNumber[3] = 2;
            }
            runInAction(() => this.roadMapCtrl.history.push(dieNumber));
            // this.togglePlayerPair.isChecked = this.toggleBankerPair.isChecked = false;
        });

        // Tie
        this.btnTie.node.on('click', () => {
            let dieNumber = [1, 1, 2, 2, 0, 0];
            if (this.togglePlayerPair.isChecked && this.toggleBankerPair.isChecked) {
                dieNumber = [1, 1, 1, 1, 0, 0];
            } else if (this.togglePlayerPair.isChecked) {
                dieNumber = [2, 1, 2, 3, 0, 0];
            } else if (this.toggleBankerPair.isChecked) {
                dieNumber = [1, 2, 3, 2, 0, 0];
            }
            runInAction(() => this.roadMapCtrl.history.push(dieNumber));
            // this.togglePlayerPair.isChecked = this.toggleBankerPair.isChecked = false;
        });

        // ask road Player
        this.btnAskRoadPlayer.node.on('click', () => {
            const askData = [2, 1, 3, 2, 0, 0];
            runInAction(() => {
                this._isAsking = true;
                this.roadMapCtrl.history.push(askData);
                this.scheduleOnce(() => {
                    this._doAskRoadFlashAni(() => {
                        runInAction(() => {
                            this.roadMapCtrl.history = this.roadMapCtrl.history.filter((el) => toJS(el) !== askData);
                            this._isAsking = false;
                        });
                    });
                });
            });
        });

        // ask road Banker
        this.btnAskRoadBanker.node.on('click', () => {
            const askData = [1, 2, 2, 3, 0, 0];
            runInAction(() => {
                this._isAsking = true;
                this.roadMapCtrl.history.push(askData);
                this.scheduleOnce(() => {
                    this._doAskRoadFlashAni(() => {
                        runInAction(() => {
                            this.roadMapCtrl.history = this.roadMapCtrl.history.filter((el) => toJS(el) !== askData);
                            this._isAsking = false;
                        });
                    });
                });
            });
        });

        // Redo
        this.btnRedo.node.on('click', () => runInAction(() => this.roadMapCtrl.history.pop()));

        // Clear all
        this.btnClear.node.on('click', () => runInAction(() => (this.roadMapCtrl.history.length = 0)));

        (globalThis as any).demo = this;
    }

    private _doAskRoadFlashAni(callback: () => void): void {
        const flashTween = (node: cc.Node): cc.Tween => {
            return cc
                .tween(node)
                .stop()
                .delay(0.5)
                .repeat(3, cc.tween().to(0.2, { opacity: 0 }).to(0.2, { opacity: 255 }))
                .to(0.3, { opacity: 0 })
                .call(callback)
                .start();
        };
        const bead = this.roadMapCtrl.lastBeadResultNode;
        const big = this.roadMapCtrl.lastBigResultNode;
        const bigEye = this.roadMapCtrl.lastBigEyeResultNode;
        const small = this.roadMapCtrl.lastSmallResultNode;
        const cock = this.roadMapCtrl.lastCockroachResultNode;

        bead && flashTween(bead);
        big && flashTween(big);
        bigEye && flashTween(bigEye);
        small && flashTween(small);
        cock && flashTween(cock);
    }

    @render renderBtns(): void {
        this.node.getComponentsInChildren(cc.Button).forEach((btn) => {
            btn.interactable = !this._isAsking;
        });
    }

    @render renderTotal(): void {
        const { total } = this.roadMapCtrl;
        if (total && !this._isAsking) {
            this.totalPlayer.string = 'Player: ' + total[0].toString();
            this.totalBanker.string = 'Banker: ' + total[1].toString();
            this.totalTie.string = 'Tie: ' + total[2].toString();
        }
    }
}
