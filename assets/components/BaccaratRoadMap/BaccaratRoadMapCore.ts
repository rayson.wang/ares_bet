// 擺盤用
export enum BaccaratType {
    Red = 'Red',
    Blue = 'Blue',
}

// 百家樂-結果
export enum BResultType {
    Player = 'Player',
    Banker = 'Banker',
    Tie = 'Tie',
    None = 'None',
}

// 珠盤顯示用, 各種組合, 3+3+3+3=12
export enum BaccaratFlag {
    bitBanker = 1, // 00001
    bitPlayer = 2, // 00010
    bitTie = 4, // 00100
    bitBankerPair = 8, // 01000
    bitPlayerPair = 16, // 10000
}

// 單局結果型態
export type BaccaratOneRoundInfo = {
    winner: BResultType;
    playerPoint: number;
    bankerPoint: number;
    playerPair: boolean;
    bankerPair: boolean;
};

// 珠盤路一局結果
export type BeadRoadResult = {
    winType: BResultType;
    pos: { x: number; y: number };
    cost: number;
};

// 百家樂一局結果
export class BaccaratPlay {
    // 內部資訊: 莊, 閒, 莊閒有無對, 結果
    winner = BResultType.None;
    playerPoint: number = null;
    bankerPoint: number = null;
    playerPair: boolean = null;
    bankerPair: boolean = null;
    cost: number = null; // 珠盤/大路顯示用, BaccaratFlag 的組合而來
    tieCount = 0;
    index: number = null;

    // 初始一局的結果
    constructor(initData?: BaccaratOneRoundInfo) {
        if (initData) {
            this.bankerPair = initData.bankerPair;
            this.bankerPoint = initData.bankerPoint;
            this.playerPair = initData.playerPair;
            this.playerPoint = initData.playerPoint;

            switch (initData.winner) {
                case BResultType.Player:
                    this.winner = BResultType.Player;
                    this.cost |= BaccaratFlag.bitPlayer;
                    break;
                case BResultType.Banker:
                    this.winner = BResultType.Banker;
                    this.cost |= BaccaratFlag.bitBanker;
                    break;
                case BResultType.Tie:
                    this.winner = BResultType.Tie;
                    this.cost |= BaccaratFlag.bitTie;
                    break;
                default:
                    this.winner = BResultType.None;
            }

            if (this.bankerPair) {
                this.cost |= BaccaratFlag.bitBankerPair;
            }

            if (this.playerPair) {
                this.cost |= BaccaratFlag.bitPlayerPair;
            }
        }
    }
}

// 珠盤路
class BaccaratBeadRoad {
    constructor(maxHeight = 6) {
        this._beadStock = [];
        this._maxHeight = maxHeight;
        this._posX = 1;
        this._posY = 0;
        this._index = 0;
    }
    _beadStock: BeadRoadResult[] = null;
    _maxHeight: number = null;
    _posX: number = null;
    _posY: number = null;
    _index: number = null;

    play(play: BaccaratPlay) {
        // 超過最大高度, 換行
        this._posY++;
        if (this._posY > this._maxHeight) {
            this._posY -= this._maxHeight;
            this._posX += 1;
        }

        // 結果堆起來
        this._beadStock.push({
            cost: play.cost,
            winType: play.winner,
            pos: { x: this._posX, y: this._posY },
        });
        this._index++;
    }

    get result() {
        // 回傳產串結果
        return this._beadStock;
    }

    clear(): void {
        this._beadStock.length = 0;
        this._posX = 0;
        this._posY = 0;
        this._index = 0;
    }
}

type PositionID = string;

// 百家樂資料
class BaccaratRoadData {
    _lastPos: any = {};
    _stocker: any = {};
    _x = 0;
    _y = 0;
    _lastPlay = new BaccaratPlay();

    get result() {
        return this._stocker;
    }

    play(play: BaccaratPlay) {
        if (this._lastPlay.winner === BResultType.None || this._lastPlay.winner === BResultType.Tie) {
            this._x = 1;
            this._y = 1;
            if (play.winner !== BResultType.Tie) {
                play.tieCount = this._lastPlay.tieCount;
                this._stocker[this._x + '_' + this._y] = play;
                this._lastPlay = play;
            } else {
                // 首局為和局
                play.tieCount = this._lastPlay.tieCount + 1;
                play.playerPair = play.bankerPair = false; // 首局合局不計入對子資訊
                this._lastPlay = play;
                this._stocker[this._x + '_' + this._y] = play;
            }
            return 0;
        }

        if (play.winner === BResultType.Tie) {
            this._lastPlay.tieCount++;
            return;
        }

        if (play.winner === this._lastPlay.winner) {
            this._y++;
            this._stocker[this._x + '_' + this._y] = play;
            this._lastPlay = play;
            return;
        }

        if (play.winner !== this._lastPlay.winner) {
            this._lastPos[this._x] = this._y;
            this._x++;
            this._y = 1;
            this._stocker[this._x + '_' + this._y] = play;
            this._lastPlay = play;
            return;
        }
    }

    getPlayById(id: string): BaccaratPlay {
        return this._stocker[id];
    }

    clear(): void {
        this._lastPos = {};
        this._stocker = {};
        this._x = 0;
        this._y = 0;
        this._lastPlay = new BaccaratPlay();
    }
}

export type routeResult = Record<string, { color: BaccaratType; ref: BaccaratPlay }>;

// 百家樂路徑 (轉彎邏輯, 判斷 BaccaratRoadData color)
class BaccaratRouting {
    constructor(maxHeight = 6) {
        this.maxHeight = maxHeight;
    }

    maxHeight: number = null;
    stock: routeResult = {};
    colorStack: BaccaratType[] = [];
    x = 0;
    y = 0;
    newLineX = 1;
    lastColor: BaccaratType = null;
    index = 0;

    get result() {
        return this.stock;
    }

    get lastResult() {
        return this.stock[this.x + '_' + this.y];
    }

    setType(color: BaccaratType, ref: BaccaratPlay): string {
        /* eslint-disable */
        const $ = this;
        $.index++;

        this.colorStack.push(color);

        // 第一步
        if ($.x === 0 && $.y === 0) {
            firstStep(color);
            return $.x + '_' + $.y;
        }

        // 換色跳
        if (isChangeColor(color)) {
            newLine(color);
            return $.x + '_' + $.y;
        }

        // 遇牆轉
        if (isWall()) {
            turn();
            return $.x + '_' + $.y;
        }

        // 遇下轉
        if (!isEmpty($.x, $.y + 1)) {
            turn();
            return $.x + '_' + $.y;
        }

        // 遇下下同色轉
        if (isNextNextSameColor(color)) {
            turn();
            return $.x + '_' + $.y;
        }

        // 遇下左同色轉
        if (isNextLeftSameColor(color)) {
            turn();
            return $.x + '_' + $.y;
        }

        // 直落
        setPos();

        return $.x + '_' + $.y;

        function firstStep(color: BaccaratType) {
            $.x++;
            $.y++;
            $.stock[$.x + '_' + $.y] = { color, ref };
            $.lastColor = color;
        }

        function newLine(color: BaccaratType) {
            $.newLineX = getNewLineX($.newLineX);
            $.x = $.newLineX;
            $.y = 1;
            $.stock[$.x + '_' + $.y] = { color, ref };
            $.lastColor = color;
        }

        function turn() {
            $.x++;
            $.stock[$.x + '_' + $.y] = { color, ref };
        }

        function setPos() {
            $.y++;
            $.stock[$.x + '_' + $.y] = { color, ref };
        }

        function isChangeColor(color: BaccaratType) {
            return $.lastColor !== color;
        }

        function isWall() {
            return $.y + 1 > $.maxHeight;
        }

        function isNextNextSameColor(color: BaccaratType) {
            return $.stock[$.x + '_' + ($.y + 2)]?.color === color;
        }

        function isNextLeftSameColor(color: BaccaratType) {
            return $.stock[$.x - 1 + '_' + ($.y + 1)]?.color === color;
        }

        function isEmpty(x: number, y: number) {
            return $.stock[x + '_' + y] === undefined;
        }

        function getNewLineX(pos: number): number {
            if (isEmpty(pos + 1, 1)) {
                return pos + 1;
            }
            return getNewLineX(pos + 1);
        }
    }

    clear(): void {
        this.stock = {};
        this.x = 0;
        this.y = 0;
        this.newLineX = 1;
        this.lastColor = null;
        this.index = 0;
        this.colorStack.length = 0;
    }
}

// 下三路紅藍判定 (使用 BaccaratRoadData 資料)
// (有對寫紅，無對寫藍，齊腳跳寫紅，突腳跳寫藍，突腳連寫紅)
class BaccaratRoadMapCore {
    /**
     * @param offsetX 1 = BigEye, 2 = Small, 3 = Cock
     */
    constructor(BRData: BaccaratRoadData, a = '2_2', b = '3_1', offsetX = 1) {
        this.BRData = BRData;
        this.offsetX = offsetX;
        this.limitA = a;
        this.limitB = b;
    }
    BRRouting = new BaccaratRouting();
    BRData: BaccaratRoadData = null;
    limitA: string = null;
    limitB: string = null;
    offsetX: number = null;

    get result() {
        return this.BRRouting.result;
    }

    get lastResult() {
        return this.BRRouting.lastResult;
    }

    play(play: BaccaratPlay) {
        if (play.winner === BResultType.Tie) {
            return;
        }

        const stock = this.BRData.result;
        const x = this.BRData._x;
        const y = this.BRData._y;

        if (stock[this.limitA] === undefined && stock[this.limitB] === undefined) {
            // console.log('#pass' + x + '_' + y);
            return;
        }

        // 齊腳跳紅,突腳跳藍
        if (y === 1) {
            if (this.BRData._lastPos[x - (this.offsetX + 1)] === this.BRData._lastPos[x - 1]) {
                this.BRRouting.setType(BaccaratType.Red, play);
            } else {
                this.BRRouting.setType(BaccaratType.Blue, play);
            }
            return;
        }

        // 有對紅,
        if (stock[x + '_' + y] && stock[x - this.offsetX + '_' + y]) {
            this.BRRouting.setType(BaccaratType.Red, play);
            return;
        }

        //連腳紅
        if (stock[x - this.offsetX + '_' + y] === undefined && stock[x - this.offsetX + '_' + (y - 1)] === undefined) {
            this.BRRouting.setType(BaccaratType.Red, play);
            return;
        }

        // 無對藍
        this.BRRouting.setType(BaccaratType.Blue, play);
    }

    clear(): void {
        this.BRRouting.clear();
        this.BRData.clear();
    }
}

// 結果計數器
class BaccaratResultCounter {
    player = 0;
    banker = 0;
    tie = 0;
    ppair = 0;
    bpair = 0;

    clear(): void {
        this.player = 0;
        this.banker = 0;
        this.tie = 0;
        this.ppair = 0;
        this.bpair = 0;
    }
}

// 百家樂路單組
export class BaccaratRoadMap {
    BRData = new BaccaratRoadData(); // 百家樂資料
    BRBead = new BaccaratBeadRoad(); // 珠盤
    BRBig = new BaccaratRouting(); // 大路 (原始)
    BRBigData: Record<string, BaccaratPlay> = {}; // 大路 (遇和局合併)
    BRBigEyeRoad = new BaccaratRoadMapCore(this.BRData, '2_2', '3_1', 1); // 大眼路
    BRSmallRoad = new BaccaratRoadMapCore(this.BRData, '3_2', '4_1', 2); // 小路
    BRCockRoad = new BaccaratRoadMapCore(this.BRData, '4_2', '5_1', 3); // 曱甴路
    resultCounter = new BaccaratResultCounter();
    index: 0;

    setResultHistory(history: BaccaratOneRoundInfo[] = []) {
        if (!history) return;
        this.clear();
        for (const play of history) this.recordPlay(play);
    }

    private recordPlay(oneRoundInfo: BaccaratOneRoundInfo): void {
        const play = new BaccaratPlay(oneRoundInfo);
        this.index++;
        play.index = this.index;
        this.BRData.play(play);
        this.BRBead.play(play);

        this.BigRoad(play);

        this.BRBigEyeRoad.play(play);
        this.BRSmallRoad.play(play);
        this.BRCockRoad.play(play);

        if( oneRoundInfo.bankerPair){
            this.resultCounter.bpair++;
        }
       
        if( oneRoundInfo.playerPair){
            this.resultCounter.ppair++;
        }

        switch (oneRoundInfo.winner) {
            case BResultType.Player: {
                this.resultCounter.player++;
                break;
            }
            case BResultType.Banker: {
                this.resultCounter.banker++;
                break;
            }
            case BResultType.Tie: {
                this.resultCounter.tie++;
                break;
            }
        }
    }

    get result() {
        return this.BRData.result;
    }

    get resultBead() {
        return this.BRBead.result;
    }

    get resultBigRoad(): routeResult {
        return this.BRBig.result;
    }

    get resultBigRoadData() {
        return this.BRBigData;
    }

    get resultBigEyeRoad(): routeResult {
        return this.BRBigEyeRoad.result;
    }

    get resultSmallRoad(): routeResult {
        return this.BRSmallRoad.result;
    }

    get resultCockRoad(): routeResult {
        return this.BRCockRoad.result;
    }

    get lastResultBigEyeRoad() {
        return this.BRBigEyeRoad.lastResult;
    }

    get lastResultSmallRoad() {
        return this.BRSmallRoad.lastResult;
    }

    get lastResultCockRoad() {
        return this.BRCockRoad.lastResult;
    }

    clear() {
        this.BRBead.clear();
        this.BRBig.clear();
        this.BRBigData = {};
        this.BRData.clear();
        this.BRBigEyeRoad.clear();
        this.BRSmallRoad.clear();
        this.BRCockRoad.clear();
        this.resultCounter.clear();
        this.index = 0;
    }

    BigRoad(play: BaccaratPlay) {
        switch (play.winner) {
            case BResultType.Banker: {
                const id = this.BRBig.setType(BaccaratType.Red, play);
                this.BRBigData[id] = play;
                break;
            }
            case BResultType.Player: {
                const id = this.BRBig.setType(BaccaratType.Blue, play);
                this.BRBigData[id] = play;
                break;
            }
            case BResultType.Tie: {
                if (this.BRData._lastPlay.winner === BResultType.Tie) {
                    this.BRBigData[
                        Object.keys(this.BRData._stocker).find(
                            (key) => this.BRData._stocker[key] === this.BRData._lastPlay,
                        )
                    ] = this.BRData._lastPlay;
                }

                break;
            }
            default:
        }
    }
}

(globalThis as any).BaccaratRoadMap = BaccaratRoadMap;
