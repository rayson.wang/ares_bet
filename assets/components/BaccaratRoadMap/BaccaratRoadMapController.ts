import { $enum, computed, observable, observer, reaction, render } from '../../shared_frameworks/SharedFrameworksIndex';
import {
    BaccaratFlag,
    BaccaratOneRoundInfo,
    BaccaratPlay,
    BaccaratRoadMap,
    BaccaratType,
    BeadRoadResult,
    BResultType,
    routeResult,
} from './BaccaratRoadMapCore';

const { ccclass, property } = cc._decorator;

// dieNumber: [ P1, B1, P2, B2, P3, B3 ]

/** suit (cardId)
    1~13 : spade
    14~26 : heart
    27~39 : diamond
    40~52 : club
*/

type dieNumber = number[];

@ccclass
@observer
export default class BaccaratRoadMapController extends cc.Component {
    baccaratRoadMap = new BaccaratRoadMap(); // 百家樂路單組
    @observable.shallow history: dieNumber[] = []; // 歷史紀錄

    @property({ type: cc.Integer, tooltip: '珠盤路格數' }) initBeadPlateNum = 36;
    @property({ type: cc.Integer, tooltip: '大路行列數' }) initBigColumnNum = 18;
    @property({ type: cc.Integer, tooltip: '大眼路行列數' }) initBigEyeColumnNum = 12;
    @property({ type: cc.Integer, tooltip: '小路行列數' }) initSmallColumnNum = 12;
    @property({ type: cc.Integer, tooltip: '蟑螂路行列數' }) initCockroachColumnNum = 12;

    lastBeadResultNode: cc.Node = null;
    lastBigResultNode: cc.Node = null;
    lastBigEyeResultNode: cc.Node = null;
    lastSmallResultNode: cc.Node = null;
    lastCockroachResultNode: cc.Node = null;

    // scrollView of roads
    @property(cc.ScrollView) scrollViewBead: cc.ScrollView = null;
    @property(cc.ScrollView) scrollViewBig: cc.ScrollView = null;
    @property(cc.ScrollView) scrollViewBigEye: cc.ScrollView = null;
    @property(cc.ScrollView) scrollViewSmall: cc.ScrollView = null;
    @property(cc.ScrollView) scrollViewCockroach: cc.ScrollView = null;

    // templates
    @property(cc.Prefab) templateBead: cc.Prefab = null;
    @property(cc.Prefab) templateBig: cc.Prefab = null;
    @property(cc.Prefab) templateBigEye: cc.Prefab = null;
    @property(cc.Prefab) templateSmall: cc.Prefab = null;
    @property(cc.Prefab) templateCockroach: cc.Prefab = null;

    // textures
    @property([cc.SpriteFrame]) texturesBead: cc.SpriteFrame[] = [];
    @property([cc.SpriteFrame]) texturesBig: cc.SpriteFrame[] = [];
    @property([cc.SpriteFrame]) texturesBigEye: cc.SpriteFrame[] = [];
    @property([cc.SpriteFrame]) texturesSmall: cc.SpriteFrame[] = [];
    @property([cc.SpriteFrame]) texturesCockroach: cc.SpriteFrame[] = [];

    onLoad(): void {
        super.onLoad?.();

        (globalThis as any).test = this;

        this.registerEvent();
    }
    registerEvent(): void {
        this.scrollViewBead.node.on('scrolling', this.updateBeadRoadView, this);
        this.scrollViewBig.node.on('scrolling', this.updateBigRoadView, this);
        this.scrollViewBigEye.node.on('scrolling', this.updateBigEyeRoadView, this);
        this.scrollViewSmall.node.on('scrolling', this.updateSmallRoadView, this);
        this.scrollViewCockroach.node.on('scrolling', this.updateCockRoachRoadView, this);
    }

    toPlayerDieNumbers(dieNumber: dieNumber): dieNumber {
        return [0, 2, 4].map((index) => (index < dieNumber.length ? dieNumber[index] : 0));
    }

    toBankerDieNumbers(dieNumber: dieNumber): dieNumber {
        return [1, 3, 5].map((index) => (index < dieNumber.length ? dieNumber[index] : 0));
    }

    toPokerPoint(cardId: number): number {
        const point = !cardId ? 0 : cardId % 13 || 13;
        return point >= 10 ? 0 : point;
    }

    getPlayerPoint(dieNumber: dieNumber): number {
        const num = [0, 2, 4].reduce((acc, it) => (acc += this.toPokerPoint(dieNumber[it])), 0);
        return num % 10;
    }

    getBankerPoint(dieNumber: dieNumber): number {
        const num = [1, 3, 5].reduce((acc, it) => (acc += this.toPokerPoint(dieNumber[it])), 0);
        return num % 10;
    }

    isPair(dieNumber: dieNumber): boolean {
        return dieNumber.length < 2 ? false : dieNumber[0] !== 0 && dieNumber[0] % 13 === dieNumber[1] % 13;
    }

    // convert dieNumber to baccaratOneRoundInfo
    toOneRoundInfo(dieNumber: dieNumber): BaccaratOneRoundInfo {
        const playerPoint = this.getPlayerPoint(dieNumber);
        const bankerPoint = this.getBankerPoint(dieNumber);
        const isTie = playerPoint === bankerPoint;
        return {
            winner: isTie ? BResultType.Tie : playerPoint > bankerPoint ? BResultType.Player : BResultType.Banker,
            playerPoint,
            bankerPoint,
            playerPair: this.isPair(this.toPlayerDieNumbers(dieNumber)),
            bankerPair: this.isPair(this.toBankerDieNumbers(dieNumber)),
        };
    }

    @computed({ keepAlive: true }) get roundsInfo(): BaccaratOneRoundInfo[] {
        return (this.history || []).reduce((acc, dieNumber) => {
            return !dieNumber ? acc : [...acc, this.toOneRoundInfo(dieNumber)];
        }, [] as BaccaratOneRoundInfo[]);
    }

    @computed get roadMapData(): {
        resultBead: BeadRoadResult[];
        resultBigRoadData: Record<string, BaccaratPlay>;
        resultBigEyeRoad: routeResult;
        resultSmallRoad: routeResult;
        resultCockRoad: routeResult;
    } {
        this.baccaratRoadMap.setResultHistory(this.roundsInfo);
        return {
            resultBead: this.baccaratRoadMap.resultBead,
            resultBigRoadData: this.baccaratRoadMap.BRBigData,
            resultBigEyeRoad: this.baccaratRoadMap.resultBigEyeRoad,
            resultSmallRoad: this.baccaratRoadMap.resultSmallRoad,
            resultCockRoad: this.baccaratRoadMap.resultCockRoad,
        };
    }

    @computed get total(): number[] {
        if (this.roadMapData) {
            return [
                this.baccaratRoadMap.resultCounter.player,
                this.baccaratRoadMap.resultCounter.banker,
                this.baccaratRoadMap.resultCounter.tie,
                this.baccaratRoadMap.resultCounter.ppair,
                this.baccaratRoadMap.resultCounter.bpair,
            ];
        }
        return null;
    }

    // 珠盤路
    @render renderBeadRoad(): void {
        if (!this.scrollViewBead) return;
        const { resultBead } = this.roadMapData;

        const sv = this.scrollViewBead;
        const template = this.templateBead;
        sv.content.removeAllChildren();

        // fill up cells
        const col = Math.floor(resultBead.length / 6);
        const remain = Math.floor(resultBead.length % 6);
        const maxCount = col * 6 + (remain > 0 ? 12 : 6);
        while (sv.content.childrenCount < maxCount || sv.content.childrenCount < this.initBeadPlateNum) {
            const node = cc.instantiate(template);
            node.children.forEach((n) => (n.active = false));
            // set postion.
            const newX = -105 + (Math.floor(sv.content.childrenCount / 6) + 3) * (40 + 2);
            const newY = 115 - Math.floor(sv.content.childrenCount % 6) * (40 + 2);
            node.setPosition(cc.v2(newX, newY));

            sv.content.addChild(node);
        }

        sv.content.children.forEach((node, index) => {
            const info = resultBead[index];
            if (!info) return;
            node.getChildByName('winType').active = true;

            let iconIndex = $enum(BResultType).indexOfKey(info.winType) * 4;
            if (
                (info.cost & BaccaratFlag.bitPlayerPair) === BaccaratFlag.bitPlayerPair &&
                (info.cost & BaccaratFlag.bitBankerPair) === BaccaratFlag.bitBankerPair
            ) {
                iconIndex += 3;
            } else if ((info.cost & BaccaratFlag.bitPlayerPair) === BaccaratFlag.bitPlayerPair) {
                iconIndex += 1;
            } else if ((info.cost & BaccaratFlag.bitBankerPair) === BaccaratFlag.bitBankerPair) {
                iconIndex += 2;
            }

            node.getChildByName('winType').getComponent(cc.Sprite).spriteFrame = this.texturesBead[iconIndex];
            this.lastBeadResultNode = node;
        });

        // 調整content size; // delay原因 需要在addchild下個frame再執行
        this.scheduleOnce(() => {
            const itemWidth = sv.content.children[0].width + 2;
            const mixCol = col + (remain > 0 ? 2 : 1);
            sv.content.width = itemWidth * mixCol;
            this.updateBeadRoadView();
            sv.scrollToRight(1);
        }, 0.1);
    }

    updateBeadRoadView(): void {
        const sv = this.scrollViewBead;

        const viewWidth = sv.content.parent.width;
        const leftView = -sv.getContentPosition().x - viewWidth;
        const rightView = -sv.getContentPosition().x + viewWidth;
        sv.content.children.forEach((node) => {
            node.active = false;
            if (node.position.x - 105 >= leftView && node.position.x - 105 <= rightView) {
                node.active = true;
            }
        });
    }

    // 大路
    @render renderBigRoad(): void {
        if (!this.scrollViewBig) return;
        const { resultBigRoadData } = this.roadMapData;

        const sv = this.scrollViewBig;
        const template = this.templateBig;
        const toColumnData: Record<string, BaccaratPlay>[] = Object.keys(resultBigRoadData).reduce((acc, key) => {
            const [x, y] = key.split('_').map((v) => +v - 1);
            acc[x] = acc[x] || new Array(6);
            acc[x][y] = resultBigRoadData[key];
            return acc;
        }, []);

        sv.content.removeAllChildren();

        // fill up columns
        const maxCol = toColumnData.length + 1;
        while (sv.content.childrenCount < maxCol || sv.content.childrenCount < this.initBigColumnNum) {
            const node = cc.instantiate(template);
            node.children.forEach((n) => n.children.forEach((n) => (n.active = false)));
            // set postion.
            const newX = 20 + sv.content.childrenCount * (40 + 2);
            node.setPosition(cc.v2(newX, 0));
            sv.content.addChild(node);
        }

        sv.content.children.forEach((column, colIndex) => {
            column.children.forEach((cell, rowIndex) => {
                const info = toColumnData[colIndex]?.[rowIndex];
                if (!info) {
                    cell.removeAllChildren();
                } else {
                    const winType = cell.getChildByName('winType').getComponent(cc.Sprite);
                    const tieCount = cell.getChildByName('tieCount').getComponent(cc.Label);
                    const tieSlash = cell.getChildByName('tieSlash');

                    winType.node.active = true;

                    tieCount.string = (info.tieCount > 1 ? info.tieCount : 0).toString();
                    tieCount.node.active = info.tieCount > 1;

                    let iconIndex = $enum(BResultType).indexOfKey(info.winner) * 4; //  不會有和

                    if (info.bankerPair && info.playerPair) {
                        iconIndex += 3;
                    } else if (info.bankerPair) {
                        iconIndex += 1;
                    } else if (info.playerPair) {
                        iconIndex += 2;
                    }

                    winType.spriteFrame = this.texturesBig[iconIndex];

                    tieSlash.active = info.tieCount > 0;

                    if (info.index === this.history.length) this.lastBigResultNode = cell;
                }
            });
        });

        // 調整content size; // delay原因 需要在addchild下個frame再執行
        this.scheduleOnce(() => {
            const itemWidth = sv.content.children[0].width + 2;
            sv.content.width = itemWidth * maxCol;

            this.updateBigRoadView();
            sv.scrollToRight(1);
        }, 0.1);
    }

    updateBigRoadView(): void {
        const sv = this.scrollViewBig;

        const viewWidth = sv.content.parent.width;
        const leftView = -sv.getContentPosition().x - viewWidth;
        const rightView = -sv.getContentPosition().x + viewWidth;

        sv.content.children.forEach((column) => {
            column.active = false;
            if (column.position.x - 377 >= leftView && column.position.x - 377 <= rightView) {
                column.active = true;
            }
        });
    }

    // 大眼路
    @render renderBigEyeRoad(): void {
        if (!this.scrollViewBigEye) return;
        const { resultBigEyeRoad } = this.roadMapData;

        const sv = this.scrollViewBigEye;
        const template = this.templateBigEye;
        const toColumnData: routeResult[] = Object.keys(resultBigEyeRoad).reduce((acc, key) => {
            const [x, y] = key.split('_').map((v) => +v - 1);
            acc[x] = acc[x] || new Array(6);
            acc[x][y] = resultBigEyeRoad[key];
            return acc;
        }, []);

        sv.content.removeAllChildren();

        // fill up columns
        const maxCol = toColumnData.length + 1;
        while (sv.content.childrenCount < maxCol || sv.content.childrenCount < this.initBigEyeColumnNum) {
            const node = cc.instantiate(template);
            node.children.forEach((n) => n.children.forEach((n) => (n.active = false)));

            // set postion.
            const newX = 2.5 + sv.content.childrenCount * (5 + 2);
            node.setPosition(cc.v2(newX, 0));

            sv.content.addChild(node);
        }

        sv.content.children.forEach((column, colIndex) => {
            column.children.forEach((cell, rowIndex) => {
                const result = toColumnData[colIndex]?.[rowIndex];
                if (!result) {
                    cell.removeAllChildren();
                } else {
                    const winType = cell.getChildByName('result').getComponent(cc.Sprite);
                    winType.node.active = true;
                    winType.spriteFrame = this.texturesBigEye[$enum(BaccaratType).indexOfKey(result.color)];
                    if (result.ref.index === this.history.length) this.lastBigEyeResultNode = cell;
                }
            });
        });

        // 調整content size; // delay原因 需要在addchild下個frame再執行
        this.scheduleOnce(() => {
            const itemWidth = sv.content.children[0].width + 2;
            sv.content.width = itemWidth * maxCol;

            this.updateBigEyeRoadView();
            sv.scrollToRight(1);
        }, 0.1);
    }

    updateBigEyeRoadView(): void {
        const sv = this.scrollViewBigEye;

        const viewWidth = sv.content.parent.width;
        const leftView = -sv.getContentPosition().x - viewWidth;
        const rightView = -sv.getContentPosition().x + viewWidth;

        sv.content.children.forEach((column) => {
            column.active = false;
            if (column.position.x - 41 >= leftView && column.position.x - 41 <= rightView) {
                column.active = true;
            }
        });
    }

    // 小路
    @render renderSmallRoad(): void {
        if (!this.scrollViewSmall) return;
        const { resultSmallRoad } = this.roadMapData;

        const sv = this.scrollViewSmall;
        const template = this.templateSmall;
        const toColumnData: routeResult[] = Object.keys(resultSmallRoad).reduce((acc, key) => {
            const [x, y] = key.split('_').map((v) => +v - 1);
            acc[x] = acc[x] || new Array(6);
            acc[x][y] = resultSmallRoad[key];
            return acc;
        }, []);

        sv.content.removeAllChildren();

        // fill up columns
        const maxCol = toColumnData.length + 1;
        while (sv.content.childrenCount < maxCol || sv.content.childrenCount < this.initSmallColumnNum) {
            const node = cc.instantiate(template);
            node.children.forEach((n) => n.children.forEach((n) => (n.active = false)));

            // set postion.
            const newX = 2.5 + sv.content.childrenCount * (5 + 2);
            node.setPosition(cc.v2(newX, 0));

            sv.content.addChild(node);
        }

        sv.content.children.forEach((column, colIndex) => {
            column.children.forEach((cell, rowIndex) => {
                const result = toColumnData[colIndex]?.[rowIndex];
                if (!result) {
                    cell.removeAllChildren();
                } else {
                    const winType = cell.getChildByName('result').getComponent(cc.Sprite);
                    winType.node.active = true;
                    winType.spriteFrame = this.texturesSmall[$enum(BaccaratType).indexOfKey(result.color)];
                    if (result.ref.index === this.history.length) this.lastSmallResultNode = cell;
                }
            });
        });

        // 調整content size; // delay原因 需要在addchild下個frame再執行
        this.scheduleOnce(() => {
            const itemWidth = sv.content.children[0].width + 2;
            sv.content.width = itemWidth * maxCol;

            this.updateSmallRoadView();
            sv.scrollToRight(1);
        }, 0.1);
    }

    updateSmallRoadView(): void {
        const sv = this.scrollViewSmall;

        const viewWidth = sv.content.parent.width;
        const leftView = -sv.getContentPosition().x - viewWidth;
        const rightView = -sv.getContentPosition().x + viewWidth;

        sv.content.children.forEach((column) => {
            column.active = false;
            if (column.position.x - 41 >= leftView && column.position.x - 41 <= rightView) {
                column.active = true;
            }
        });
    }

    // 蟑螂路
    @render renderCockRoachRoad(): void {
        if (!this.scrollViewCockroach) return;
        const { resultCockRoad } = this.roadMapData;

        const sv = this.scrollViewCockroach;
        const template = this.templateCockroach;
        const toColumnData: routeResult[] = Object.keys(resultCockRoad).reduce((acc, key) => {
            const [x, y] = key.split('_').map((v) => +v - 1);
            acc[x] = acc[x] || new Array(6);
            acc[x][y] = resultCockRoad[key];
            return acc;
        }, []);

        sv.content.removeAllChildren();

        // fill up columns
        while (
            sv.content.childrenCount < toColumnData.length + 1 ||
            sv.content.childrenCount < this.initCockroachColumnNum
        ) {
            const node = cc.instantiate(template);
            node.children.forEach((n) => n.children.forEach((n) => (n.active = false)));

            // set postion.
            const newX = 2.5 + sv.content.childrenCount * (5 + 2);
            node.setPosition(cc.v2(newX, 0));

            sv.content.addChild(node);
        }

        sv.content.children.forEach((column, colIndex) => {
            column.children.forEach((cell, rowIndex) => {
                const result = toColumnData[colIndex]?.[rowIndex];
                if (!result) {
                    cell.removeAllChildren();
                } else {
                    const winType = cell.getChildByName('result').getComponent(cc.Sprite);
                    winType.node.active = true;
                    winType.spriteFrame = this.texturesCockroach[$enum(BaccaratType).indexOfKey(result.color)];
                    if (result.ref.index === this.history.length) this.lastCockroachResultNode = cell;
                }
            });
        });

        // 調整content size; // delay原因 需要在addchild下個frame再執行
        this.scheduleOnce(() => {
            const itemWidth = sv.content.children[0].width + 2;
            sv.content.width = itemWidth * sv.content.childrenCount;

            this.updateCockRoachRoadView();
            sv.scrollToRight(1);
        }, 0.1);
    }

    updateCockRoachRoadView(): void {
        const sv = this.scrollViewCockroach;

        const viewWidth = sv.content.parent.width;
        const leftView = -sv.getContentPosition().x - viewWidth;
        const rightView = -sv.getContentPosition().x + viewWidth;

        sv.content.children.forEach((column) => {
            column.active = false;
            if (column.position.x - 41 >= leftView && column.position.x - 41 <= rightView) {
                column.active = true;
            }
        });
    }
}
